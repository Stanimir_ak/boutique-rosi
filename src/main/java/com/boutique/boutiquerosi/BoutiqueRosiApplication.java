package com.boutique.boutiquerosi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoutiqueRosiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoutiqueRosiApplication.class, args);
	}

}
