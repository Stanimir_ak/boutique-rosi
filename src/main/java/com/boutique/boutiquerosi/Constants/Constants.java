package com.boutique.boutiquerosi.Constants;

public class Constants {
    //ENG letters, between 3 and 20 symbols
    public static final String REGEX_INPUT_PATTERN = "^[A-Za-z]{3,20}$";
    public static final String FILE_EXTENSIONS = "pdf,xls,xlsx,doc,docx,txt,jpg,png";

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String EXCEL_EXTENSIONS = "xls,xlsx";

    public static final String FORBIDDEN_INPUT_WORDS = "select,update,delete,create,from,table,row,where,join," +
            "insert,users,user,admin,customers,products,orders,";

    public static final int PRODUCTS_PER_PAGE = 20;
    public static final int MAX_PRODUCT_PIC_SIZE_IN_BYTES = 300000; //300kb
}
