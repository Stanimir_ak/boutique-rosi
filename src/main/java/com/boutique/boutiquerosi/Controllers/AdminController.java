package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Exceptions.DuplicateEntityException;
import com.boutique.boutiquerosi.Exceptions.EntityNotFoundException;
import com.boutique.boutiquerosi.Exceptions.InvalidOperationException;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.*;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final FileSystem fileSystem;
    private final MessageService messageService;
    private final ProductPictureService productPictureService;
    private final ProductService productService;
    private final UserService userService;
    private final OrderService orderService;
    private final CategoryService categoryService;
    private final SubcategoryService subcategoryService;

    @Autowired
    public AdminController(FileSystem fileSystem, MessageService messageService, ProductPictureService productPictureService,
                           ProductService productService, UserService userService, OrderService orderService,
                           CategoryService categoryService, SubcategoryService subcategoryService) {
        this.fileSystem = fileSystem;
        this.messageService = messageService;
        this.productPictureService = productPictureService;
        this.productService = productService;
        this.userService = userService;
        this.orderService = orderService;
        this.categoryService = categoryService;
        this.subcategoryService = subcategoryService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @ModelAttribute("productPicture")
    public ProductPicture populateProductPicture() {
        return new ProductPicture();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping("")
    public String admin(Model model) {
        return "admin";
    }

    @GetMapping("/files")
    public String adminFiles(Model model) {
        return "products-update";
    }

    @PostMapping("/files/upload")
    String updateAllProductsAndCategories(@RequestParam("importedFile") MultipartFile file, Principal principal, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            fileSystem.updateAllProductsAndCategories(principal, file);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "products-update";
        }
        return "products-update";
    }

    @PostMapping("/files/uploadQuantity")
    String updateProductsQuantity(@RequestParam("importedFile") MultipartFile file, Principal principal, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            fileSystem.updateProductsQuantity(principal, file);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "products-update";
        }
        return "products-update";
    }

    @GetMapping("/productPicture")
    public String productPicture(Model model) {
        List<Product> allProducts = productService.getAllWithHidden();
        model.addAttribute("products", allProducts);
        return "products-pictures";
    }

    @PostMapping("/productPicture")
    String addPictureToProduct(@RequestParam("importedFile2") MultipartFile file, Principal principal, Model model,
                               @ModelAttribute(name = "productPicture") ProductPicture productPicture) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            if (file.getSize() > Constants.MAX_PRODUCT_PIC_SIZE_IN_BYTES) {
                model.addAttribute("error", messageService.getTranslatedMessage("product.picture.max.size"));
                return "products-pictures";
            }
            productPicture.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            productPictureService.create(productPicture);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "products-pictures";
        }
        return "redirect:/admin/productPicture/" + productPicture.getProductId();
    }

    @GetMapping("/productPicture/{id}")
    public String getProductDetails(Model model, @PathVariable(name = "id") int id,
                                    @ModelAttribute(name = "productPicture") ProductPicture productPicture) {
        try {
            Product product = productService.getOne(id);
            List<ProductPicture> productPictures = productPictureService.getAllPicOfProduct(id);

            model.addAttribute("product", product);
            model.addAttribute("productPictures", productPictures);
            productPicture.setPicNum(productPictureService.getNextNumber(productPictures));
            model.addAttribute("productPicture", productPicture);
            return "products-arrange-pic";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "products-arrange-pic";
        }
    }

    @PostMapping("/productPicture/makeFirst/{id}/{number}")
    public String makeThisFirstPicture(Model model, @PathVariable(name = "id") int id, Principal principal,
                                       @PathVariable(name = "number") int number) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            Product product = productService.getOne(id);
            List<ProductPicture> productPictures = productPictureService.getAllPicOfProduct(id);

            productPictures = productPictureService.makeThisFirstPictureAndReturn(productPictures, number);

            model.addAttribute("product", product);
            model.addAttribute("productPictures", productPictures);
            return "redirect:/admin/productPicture/{id}";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "products-arrange-pic";
        }
    }

    @PostMapping("/productPicture/deletePicture/{prodId}/{picId}")
    public String deletePicture(@PathVariable(name = "prodId") int prodId, Model model,
                                @PathVariable(name = "picId") int pictureIdToDelete, Principal principal) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            Product product = productService.getOne(prodId);
            List<ProductPicture> productPictures = productPictureService.getAllPicOfProduct(prodId);

            productPictureService.deleteAndMoveOtherPics(pictureIdToDelete, productPictures);

            productPictures = productPictureService.getAllPicOfProduct(prodId);

            model.addAttribute("product", product);
            model.addAttribute("productPictures", productPictures);
            return "redirect:/admin/productPicture/{prodId}";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "products-arrange-pic";
        }
    }

    @GetMapping("/productDelete")
    public String productDelete(Model model, Principal principal) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "products-delete";
        }

        List<Product> allProducts = productService.getAllWithHidden();
        model.addAttribute("products", allProducts);
        return "products-delete";
    }

    @PostMapping("/productDelete/{id}")
    String productDelete(Principal principal, Model model, @PathVariable(name = "id") int id) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            productService.deleteProductAndItsPictures(id);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "products-delete";
        }
        return "redirect:/admin/productDelete";
    }

    @GetMapping("/crud/createProduct")
    public String getFormCreateNewProduct(Model model) {
        Product product = new Product();
        List<Category> categories = categoryService.getAll();
        List<Subcategory> subcategories = subcategoryService.getAll();

        model.addAttribute("product", product);
        model.addAttribute("categories", categories);
        model.addAttribute("subcategories", subcategories);
        return "products-create";
    }

    @PostMapping("/crud/createProduct")
    public String createNewProduct(Model model, @ModelAttribute("product") Product product, BindingResult errors,
                                   @RequestParam(name = "imagefile3", required = false) MultipartFile file, Principal principal) {

        if (errors.hasErrors()) {
            return "products-create";
        }

        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);

            if (file != null && !file.isEmpty() && file.getSize() > Constants.MAX_PRODUCT_PIC_SIZE_IN_BYTES) {
                model.addAttribute("error", messageService.getTranslatedMessage("product.picture.max.size"));
                return "products-create";
            }

            Product theNewProduct = productService.createProduct(product);

            if (file != null && !file.isEmpty()) {
                ProductPicture productPicture = new ProductPicture();
                productPicture.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
                productPicture.setProductId(theNewProduct.getId());
                productPicture.setPicNum(1);

                productPictureService.create(productPicture);
            }

            return "redirect:/admin/crud/createProduct";
        } catch (InvalidOperationException | DuplicateEntityException | EntityNotFoundException | AccessDeniedException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        } catch (IOException ex) {
            return "products-create";
        }
    }

    @GetMapping("/crud/updateProduct/{productId}")
    public String getUpdateProduct(Model model, @PathVariable(name = "productId") int productId) {
        Product product = productService.getOne(productId);
        List<Category> categories = categoryService.getAll();
        List<Subcategory> subcategories = subcategoryService.getAll();

        model.addAttribute("product", product);
        model.addAttribute("categories", categories);
        model.addAttribute("subcategories", subcategories);
        return "product-update";
    }

    @PostMapping("/crud/updateProduct")
    public String updateProduct(Model model, @ModelAttribute(name = "product") Product product, BindingResult errors) {
        if (errors.hasErrors()) {
            return "product-update";
        }

        try {
            productService.updateProduct(product);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
//            List<Product> allProducts = productService.getAllWithHidden();
//            model.addAttribute("products", allProducts);
            return "products-pictures";
        }

        return "redirect:/admin/productPicture";
    }

    @GetMapping("/open-orders")
    public String getOpenOrders(Model model, Principal principal) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            model.addAttribute("openOrders", orderService.getAllOpen(principal));
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "openOrders";
    }

    @GetMapping("/closed-orders")
    public String getClosedOrders(Model model, Principal principal) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            model.addAttribute("closedOrders", orderService.getAllClosed(principal));
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "closedOrders";
    }

    @GetMapping("/orders")
    public String getOrders(Model model, Principal principal) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            model.addAttribute("openOrders", orderService.getAllOpen(principal));
            model.addAttribute("closedOrders", orderService.getAllClosed(principal));
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "adminOrders";
    }

    @PostMapping("/orders/closeOrder/{id}")
    public String closeOrder(Principal principal, @PathVariable(name = "id") Integer id, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            Order order = orderService.getOne(id, principal);
            orderService.closeOrder(order, principal);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "redirect:/admin/orders";
    }

    @PostMapping("/orders/deleteOrder/{id}")
    public String deleteOrder(Principal principal, @PathVariable(name = "id") Integer id, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            Order order = orderService.getOne(id, principal);

            orderService.deleteAndReturnStock(order, principal);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "redirect:/admin/orders";
    }

    @GetMapping("/users")
    public String getUsers(Principal principal, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            List<User> allUsers = userService.getAllUsers();
            model.addAttribute("users", allUsers);
        } catch (Exception ex) {
            model.addAttribute("error", ex.getMessage());
            return "redirect:/";
        }
        return "users";
    }

    @GetMapping("/downloadProductFile")
    public void downloadProductFile(HttpServletResponse response, Principal principal, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            fileSystem.downloadExcelFile(response, principal, ExcelType.ProductFile);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
        }
    }

    @GetMapping("/downloadStatisticsFile")
    public void downloadStatisticsFile(HttpServletResponse response, Principal principal, Model model) {
        try {
            userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
            fileSystem.downloadExcelFile(response, principal, ExcelType.StatisticsFile);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
        }
    }
}
