package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Services.CategoryService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/contacts")
public class ContactsController {

    private final CategoryService categoryService;

    @Autowired
    public ContactsController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @GetMapping("")
    public String getContacts(Model model) {
        try {
            return "contact";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "contact";
        }
    }
}
