package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.CategoryService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {

    private final CategoryService categoryService;

    @Autowired
    public IndexController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @GetMapping
    public String index(Model model) {
        List<Category> categories = categoryService.getAll();
        model.addAttribute("categories", categories);
        return "index";
    }
}
