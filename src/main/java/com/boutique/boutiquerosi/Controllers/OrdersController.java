package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Model.User;
import com.boutique.boutiquerosi.Services.*;
import com.boutique.boutiquerosi.Utils.Utils;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrdersController {

    private final OrderService orderService;
    private final UserService userService;
    private final MessageService messageService;
    private final ProductService productService;

    @Autowired
    public OrdersController(OrderService orderService, UserService userService, MessageService messageService,
                            ProductService productService) {
        this.orderService = orderService;
        this.userService = userService;
        this.messageService = messageService;
        this.productService = productService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        shoppingCart.getOrder().calculateAmountAndTransport();
        return shoppingCart;
    }

    @GetMapping("")
    private String showOrderDetails(@ModelAttribute(name = "shoppingCart") ShoppingCart shoppingCart, Model model) {
        if (shoppingCart == null || shoppingCart.getOrder().getSize() == 0) {
            model.addAttribute("error", messageService.getTranslatedMessage("order.empty.cart"));
            return "redirect:/shoppingCart";
        }
        return "order";
    }

    @PostMapping("/create")
    private String createOrder(HttpSession session, @ModelAttribute(name = "shoppingCart") ShoppingCart shoppingCart,
                               Principal principal, Model model) {

        if (shoppingCart == null || shoppingCart.getOrder().getSize() == 0) {
            model.addAttribute("error", messageService.getTranslatedMessage("order.empty.cart"));
            return "redirect:/shoppingCart";
        }

        if (Utils.isNotValidOrderInfo(shoppingCart.getOrder())) {
            model.addAttribute("error", messageService.getTranslatedMessage("invalid.user.input"));
            return "order";
        }

        List<Product> dbProducts = productService.getAllWithoutHidden();
        if (!orderService.isEnoughProductQuantity(shoppingCart, dbProducts)) {
            model.addAttribute("error", messageService.getTranslatedMessage("order.not.enough.quantity.orderChanged"));
            orderService.decreaseOrderQuantityWhenNotEnough(shoppingCart, dbProducts);
            return "redirect:/shoppingCart";
        }

        if (principal != null) {
            User user = userService.getByUsername(principal.getName());

            shoppingCart.getOrder().setUserId(user.getId());
            shoppingCart.getOrder().setUserName(user.getUsername());
        }

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        shoppingCart.getOrder().setDate(formatter.format(date));

        //TODO validate fields of the Order

        orderService.create(shoppingCart.getOrder(), principal);
        session.setAttribute("shoppingCart", null);

        return "order-confirmation";
    }
}
