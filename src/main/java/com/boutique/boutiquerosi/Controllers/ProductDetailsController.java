package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.CategoryService;
import com.boutique.boutiquerosi.Services.ProductPictureService;
import com.boutique.boutiquerosi.Services.ProductService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/productsDetails")
public class ProductDetailsController {

    private final ProductService productService;
    private final ProductPictureService productPictureService;
    private final CategoryService categoryService;


    @Autowired
    public ProductDetailsController(ProductService productService,
                                    ProductPictureService productPictureService,
                                    CategoryService categoryService) {

        this.productService = productService;
        this.productPictureService = productPictureService;
        this.categoryService = categoryService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public String getProductDetails(Model model, @PathVariable(name = "id") int id) {
        try {
            Product product = productService.getOne(id);
            List<ProductPicture> productPictures = productPictureService.getAllPicOfProduct(id);

            model.addAttribute("product", product);
            model.addAttribute("productPictures", productPictures);
            return "portfolio-details";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "portfolio-details";
        }
    }
}
