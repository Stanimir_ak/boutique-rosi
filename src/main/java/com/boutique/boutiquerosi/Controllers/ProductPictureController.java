package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.ProductPicture;
import com.boutique.boutiquerosi.Services.ProductPictureService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

@Controller
@RequestMapping("/productPicture")
public class ProductPictureController {

    private final ProductPictureService productPictureService;

    @Autowired
    public ProductPictureController(ProductPictureService productPictureService) {
        this.productPictureService = productPictureService;
    }

    @GetMapping("/{prod_id}/{pic_num}")
    public void renderItemImageFormDB(@PathVariable int prod_id, @PathVariable int pic_num,
                                      HttpServletResponse response) throws IOException {
        ProductPicture productPicture = productPictureService.getOneByProdIdAndPicNum(prod_id, pic_num);
        if (productPicture != null && productPicture.getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(productPicture.getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        } else {
            response.setContentType("image/jpeg");
            InputStream is = new FileInputStream("src/main/resources/static/images/default.jpg");
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}
