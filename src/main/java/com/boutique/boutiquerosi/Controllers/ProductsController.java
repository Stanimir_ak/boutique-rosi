package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.CategoryService;
import com.boutique.boutiquerosi.Services.MessageService;
import com.boutique.boutiquerosi.Services.ProductService;
import com.boutique.boutiquerosi.Utils.Utils;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductsController {

    private final ProductService productService;
    private final CategoryService categoryService;
    private final MessageService messageService;

    @Autowired
    public ProductsController(ProductService productService, CategoryService categoryService, MessageService messageService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.messageService = messageService;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("showSearch")
    public boolean populateShowSearch() {
        return true;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @ModelAttribute("page")
    public Page populatePage() {
        Page page = new Page();
        page.setIndex(1);
        page.setSize(Constants.PRODUCTS_PER_PAGE);
        page.setKeyWords("");
        return page;
    }

    @GetMapping("")
    public String getProducts(Model model, @ModelAttribute(name = "page") Page page) {
        Slice<Product> allProducts;
        if (!page.getKeyWords().equals("")) {
            if (Utils.isNotValidInput(page.getKeyWords())) {
                model.addAttribute("error", messageService.getTranslatedMessage("invalid.user.input"));
                return "portfolio";
            }

            allProducts = productService.search(page.getKeyWords(), page);

            model.addAttribute("products", allProducts);
            model.addAttribute("hasNext", allProducts.hasNext());
            model.addAttribute("hasPrevious", page.getIndex() != 1);
        } else {
            allProducts = productService.getAllSlicedWithoutHidden(page);
        }

        model.addAttribute("products", allProducts);
        model.addAttribute("hasNext", allProducts.hasNext());
        model.addAttribute("hasPrevious", page.getIndex() != 1);
        return "portfolio";
    }

    @GetMapping("/new")
    public String getAllNew(Model model, @ModelAttribute(name = "page") Page page) {
        Slice<Product> newProducts = productService.getAllNew(page);

        model.addAttribute("products", newProducts);
        model.addAttribute("hasNext", newProducts.hasNext());
        model.addAttribute("hasPrevious", page.getIndex() != 1);
        model.addAttribute("showSearch", false);
        return "portfolio";
    }

    @GetMapping("/inAction")
    public String getAllInAction(Model model, @ModelAttribute(name = "page") Page page) {
        Slice<Product> inActionProducts = productService.getAllInAction(page);

        model.addAttribute("products", inActionProducts);
        model.addAttribute("hasNext", inActionProducts.hasNext());
        model.addAttribute("hasPrevious", page.getIndex() != 1);
        model.addAttribute("showSearch", false);
        return "portfolio";
    }

    @GetMapping("/{category}")
    public String getProductsByCategory(@PathVariable(name = "category") String category,
                                        @ModelAttribute(name = "page") Page page, Model model) {
        Category category1 = categoryService.getOneByName(category);
        Slice<Product> allProducts;

        if (!page.getKeyWords().isEmpty()) {
            allProducts = productService.getProductsByCategoryAndSearch(category, page.getKeyWords(), page);
        } else {
            allProducts = productService.getAllByCategory(category, page);
        }

        model.addAttribute("products", allProducts);
        model.addAttribute("category", category1);
        model.addAttribute("hasNext", allProducts.hasNext());
        model.addAttribute("hasPrevious", page.getIndex() != 1);
        return "oneCategory";
    }
}
