package com.boutique.boutiquerosi.Controllers.REST;

import com.boutique.boutiquerosi.Model.ChangeQuantityDTO;
import com.boutique.boutiquerosi.Model.ProductDTO;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Services.ProductService;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/rest")
public class RestShoppingCartController {

    private final ProductService productService;

    public RestShoppingCartController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/cart/quantityUP")
    public ChangeQuantityDTO quantityUP(HttpSession session,
                                        @RequestParam int productId) {

        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");

        updateProductInCartIncrease(shoppingCart, productId, 1);
        return new ChangeQuantityDTO(shoppingCart, getProductDTO(shoppingCart, productId));
    }

    @PostMapping("/cart/quantityDOWN")
    public ChangeQuantityDTO quantityDOWN(HttpSession session,
                                          @RequestParam(name = "productId") int productId) {

        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");

        updateProductInCartDecrease(shoppingCart, productId, -1);
        return new ChangeQuantityDTO(shoppingCart, getProductDTO(shoppingCart, productId));
    }

    private void updateProductInCartIncrease(ShoppingCart shoppingCart, int productId, int diff) {
        for (ProductDTO product : shoppingCart.getOrder().getProductList()) {
            if (product.getProductId() == productId) {
                if (productService.isEnoughProductQuantityOnIncreaseWithOne(productId, shoppingCart)) {
                    product.setOrderedQuantity(product.getOrderedQuantity() + diff * product.getSinglePackage());
                    shoppingCart.getOrder().calculateAmountAndTransport();
                }
                break;
            }
        }
    }

    private void updateProductInCartDecrease(ShoppingCart shoppingCart, int productId, int diff) {
        for (ProductDTO product : shoppingCart.getOrder().getProductList()) {
            if (product.getProductId() == productId) {
                if (product.getOrderedQuantity() <= product.getSinglePackage()) {
                    return;
                }
                product.setOrderedQuantity(product.getOrderedQuantity() + (diff * product.getSinglePackage()));
                shoppingCart.getOrder().calculateAmountAndTransport();
                break;
            }
        }
    }

    private ProductDTO getProductDTO(ShoppingCart shoppingCart, int productId) {
        for (ProductDTO product : shoppingCart.getOrder().getProductList()) {
            if (product.getProductId() == productId) {
                return product;
            }
        }
        return null;
    }
}
