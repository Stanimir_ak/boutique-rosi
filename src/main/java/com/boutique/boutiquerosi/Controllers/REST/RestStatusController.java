package com.boutique.boutiquerosi.Controllers.REST;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestStatusController {

    @GetMapping("/status")
    public String status() {
        return "Status Rossi-Art: Up and running";
    }
}
