package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.CategoryService;
import com.boutique.boutiquerosi.Services.MessageService;
import com.boutique.boutiquerosi.Services.RoleService;
import com.boutique.boutiquerosi.Services.UserService;
import com.boutique.boutiquerosi.Utils.Utils;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class RegistrationController {
    private final UserDetailsManager userDetailsManager;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final MessageService messageService;
    private final CategoryService categoryService;
    private final UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, RoleService roleService, PasswordEncoder passwordEncoder, MessageService messageService, CategoryService categoryService, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.messageService = messageService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @GetMapping("/sign-up")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "sign-up";
    }

    @PostMapping("/sign-up")
    public String registerUser(@ModelAttribute User user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", messageService.getTranslatedMessage("user.pass.cannotBeLessThan2"));
            return "sign-up";
        }

        if (Utils.isNotValidUserInfo(user)) {
            model.addAttribute("error", messageService.getTranslatedMessage("invalid.user.input"));
            return "sign-up";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", messageService.getTranslatedMessage("user.already.exists"));
            return "sign-up";
        }

        if (!user.getUsername().matches(Constants.REGEX_INPUT_PATTERN) || !user.getPassword().matches(Constants.REGEX_INPUT_PATTERN)) {
            model.addAttribute("error", messageService.getTranslatedMessage("user.pass.langAndLengthMsg"));
            return "sign-up";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", messageService.getTranslatedMessage("pass.does.not.match"));
            return "sign-up";
        }

        if (!roleService.existsByName(Constants.ROLE_USER)) {
            roleService.create(new Role(Constants.ROLE_USER));
        }
        if (!roleService.existsByName(Constants.ROLE_ADMIN)) {
            roleService.create(new Role(Constants.ROLE_ADMIN));
        }

        User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()));
        newUser.addAuthority(new Role("ROLE_USER"));
        newUser.setEnabled(true);
        userDetailsManager.createUser(newUser);
        return "sign-up-confirmation";
    }

    @GetMapping("/sign-up-confirmation")
    public String showRegisterConfirmation() {
        return "sign-up-confirmation";
    }

    @GetMapping("/changePassword")
    public String getChangePassword(Model model, Principal principal) {
        User userToUpdate = new User();
        model.addAttribute("userToUpdate", userToUpdate);
        return "change-password";
    }

    @PostMapping("/changePassword")
    public String changePassword(Model model, Principal principal,
                                 @ModelAttribute("userToUpdate") User userToUpdate) {
        if (Utils.isNotValidUserInfo(userToUpdate)) {
            model.addAttribute("error", messageService.getTranslatedMessage("invalid.user.input"));
            return "change-password";
        }

        try {
            userService.changePassword(principal, userToUpdate);
            return "change-pass-confirmation";
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return "change-password";
        }
    }
}
