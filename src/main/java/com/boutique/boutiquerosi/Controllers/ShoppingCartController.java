package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.CategoryService;
import com.boutique.boutiquerosi.Services.MessageService;
import com.boutique.boutiquerosi.Services.ProductService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    private final CategoryService categoryService;
    private final ProductService productService;
    private final MessageService messageService;

    @Autowired
    public ShoppingCartController(CategoryService categoryService, ProductService productService, MessageService messageService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.messageService = messageService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        shoppingCart.getOrder().calculateAmountAndTransport();
        return shoppingCart;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping
    public String getShoppingCart(Model model) {
        return "shoppingCart";
    }

    @PostMapping("/add/{productId}")
    public String addToShoppingCart(HttpSession session, @PathVariable(name = "productId") int productId,
                                    Model model) {
        Product product = productService.getOne(productId);
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");

        if (productAlreadyAdded(productId, shoppingCart.getOrder())) {
            model.addAttribute("error", messageService.getTranslatedMessage("product.already.added"));
            return "shoppingCart";
        }

        if (product.isHidden()) {
            return "redirect:/products";
        }

        if (!productService.isEnoughProductQuantityOnIncreaseWithOne(productId, shoppingCart)) {
            model.addAttribute("error", messageService.getTranslatedMessage("order.not.enough.quantity"));
            return "redirect:/products";
        }

        ProductDTO productDTO = productService.createDTO(product);

        shoppingCart.getOrder().addProduct(productDTO);
        return "redirect:/shoppingCart";
    }

    @PostMapping("/remove/{productId}")
    public String removeFromShoppingCart(HttpSession session, @PathVariable(name = "productId") int productId,
                                         Model model) {
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        for (ProductDTO product : shoppingCart.getOrder().getProductList()) {
            if (product.getProductId() == productId) {
                shoppingCart.getOrder().removeProduct(product);
                break;
            }
        }

        return "redirect:/shoppingCart";
    }

    private boolean productAlreadyAdded(int productId, Order order) {
        for (ProductDTO productDTO : order.getProductList()) {
            if (productDTO.getProductId() == productId) {
                return true;
            }
        }
        return false;
    }
}
