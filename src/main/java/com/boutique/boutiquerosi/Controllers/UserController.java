package com.boutique.boutiquerosi.Controllers;

import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Services.CategoryService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

    private final CategoryService categoryService;

    @Autowired
    public UserController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @GetMapping("/profile")
    public String getUserProfile(Principal principal) {
        if (principal == null) {
            return "redirect:/";
        }

        return "user-profile";
    }
}
