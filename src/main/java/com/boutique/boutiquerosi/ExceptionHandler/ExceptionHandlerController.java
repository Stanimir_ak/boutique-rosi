package com.boutique.boutiquerosi.ExceptionHandler;

import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.ProductPicture;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Services.CategoryService;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
@Slf4j
public class ExceptionHandlerController {
    private CategoryService categoryService;

    @Autowired
    public ExceptionHandlerController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ModelAttribute("shoppingCart")
    public ShoppingCart populateShoppingCart(HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
            return shoppingCart;
        }
        shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        return shoppingCart;
    }

    @ModelAttribute("productPicture")
    public ProductPicture populateProductPicture() {
        return new ProductPicture();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ExceptionHandler(Exception.class)  //handle this exception
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public String storageException(final Exception e, final Model model, HttpSession session) {
        ShoppingCart shoppingCart;
        if (session.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart(new Order());
            session.setAttribute("shoppingCart", shoppingCart);
        } else {
            shoppingCart = (ShoppingCart) session.getAttribute("shoppingCart");
        }

        model.addAttribute("shoppingCart", shoppingCart);
        model.addAttribute("categories", categoryService.getAll());

        model.addAttribute("errorMessage", e.getMessage()); //custom message to render in HTML
        return "error";  //the html page in resources/templates folder
    }
}
