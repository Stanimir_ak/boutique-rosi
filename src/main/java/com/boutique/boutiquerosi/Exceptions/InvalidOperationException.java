package com.boutique.boutiquerosi.Exceptions;

public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException(String s) {
        super(s);
    }

    public InvalidOperationException() {
        super(String.format("Invalid operation!"));
    }
}
