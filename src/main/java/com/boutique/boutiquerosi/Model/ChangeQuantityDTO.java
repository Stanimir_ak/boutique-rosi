package com.boutique.boutiquerosi.Model;

public class ChangeQuantityDTO {
    private ShoppingCart cart;
    private ProductDTO productDTO;

    public ChangeQuantityDTO(ShoppingCart cart, ProductDTO productDTO) {
        this.cart = cart;
        this.productDTO = productDTO;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }
}
