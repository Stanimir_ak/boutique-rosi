package com.boutique.boutiquerosi.Model;

import com.boutique.boutiquerosi.Utils.Utils;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "city")
    private String city;

    @Column(name = "address")
    private String address;

    @Column(name = "date")
    private String date;

    @Column(name = "transport_costs")
    private double transportCosts;

    @Column(name = "amount")
    private double amount;

    @Column(name = "isClosed")
    private boolean isClosed = false;

    @OneToMany(mappedBy = "order")
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonBackReference
    private List<ProductDTO> productList = new ArrayList<>();

    @Column(name = "size")
    private int size;

    public Order() {
    }

    public Order(int id, int userId, String userName, String phone, String city, String address, String date,
                 double transportCosts, double amount, boolean isClosed, int size) {
        this.id = id;
        this.userId = userId;
        this.userName = userName;
        this.phone = phone;
        this.city = city;
        this.address = address;
        this.date = date;
        this.transportCosts = transportCosts;
        this.amount = amount;
        this.isClosed = isClosed;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTransportCosts() {
        return Utils.round(transportCosts, 2);
    }

    public void setTransportCosts(double transportCosts) {
        this.transportCosts = transportCosts;
    }

    public double getAmount() {
        return Utils.round(amount, 2);
    }

    public void setAmount(double amount) {
        this.amount = Utils.round(amount, 2);
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
        calculateAmountAndTransport();
        this.size = productList.size();
    }

    public void addProduct(ProductDTO productDTO) {
        this.productList.add(productDTO);
        this.size++;
        calculateAmountAndTransport();
    }

    public void removeProduct(ProductDTO productDTO) {
        this.productList.remove(productDTO);
        this.size--;
        calculateAmountAndTransport();
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public void calculateAmountAndTransport() {
        this.amount = 0;
        for (ProductDTO productDTO : productList) {
            this.amount += Utils.round(productDTO.getPrice() / productDTO.getSinglePackage()
                    * productDTO.getOrderedQuantity(), 2);
        }
        if (productList.size() == 0) {
            this.transportCosts = 0.0;
        } else {
            if (this.amount < 80.0) {
                this.transportCosts = 5.0;
                this.amount += getTransportCosts();
            } else {
                this.transportCosts = 0.0;
            }
        }
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
