package com.boutique.boutiquerosi.Model;

import jakarta.persistence.*;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "subcategory_id", referencedColumnName = "subcategory_id")
    private Subcategory subcategory;

    @Column(name = "price")
    private double price;

    @Column(name = "package")
    private int singlePackage;

    @Column(name = "info")
    private String info;

    @Column(name = "is_new")
    private boolean isNew = false;

    @Column(name = "is_action")
    private boolean isAction = false;

    @Column(name = "is_hidden")
    private boolean isHidden = false;

    @Column(name = "quantity")
    private int quantity = 0;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSinglePackage() {
        return singlePackage;
    }

    public void setSinglePackage(int singlePackage) {
        this.singlePackage = singlePackage;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isNew() {
        return isNew;
    }

    public boolean getIsNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isAction() {
        return isAction;
    }

    public boolean getIsAction() {
        return isAction;
    }

    public void setAction(boolean action) {
        isAction = action;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public boolean getIsHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public boolean equals(Product obj) {
        return this.name.equals(obj.getName()) &&
                this.category.getName().equals(obj.getCategory().getName()) &&
                this.subcategory.getName().equals(obj.getSubcategory().getName()) &&
                this.price == obj.getPrice() &&
                this.singlePackage == obj.getSinglePackage() &&
                this.info.equals(obj.getInfo()) &&
                this.isNew == obj.isNew() &&
                this.isAction == obj.isAction() &&
                this.isHidden == obj.isHidden();

        //Quantity is not considered part of the product. I am updating it in separate service
//                this.quantity == obj.quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
