package com.boutique.boutiquerosi.Model;

import com.boutique.boutiquerosi.Utils.Utils;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
@Table(name = "order_products")
public class ProductDTO {

    @Id
    @Column(name = "product_dto_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @JsonBackReference
    private Order order;

    @Column(name = "name")
    private String name;

    @Column(name = "product_id")
    private int productId;

    @Column(name = "price")
    private double price;

    @Column(name = "ordered_quantity")
    private int orderedQuantity;

    @Column(name = "package")
    private int singlePackage;

    @Transient
    private double amount;

    public ProductDTO() {
    }

    public ProductDTO(Order order, int productId, double price, int orderedQuantity, int singlePackage) {
        this.order = order;
        this.productId = productId;
        this.price = price;
        this.orderedQuantity = orderedQuantity;
        this.singlePackage = singlePackage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return Utils.round(price, 2);
    }

    public void setPrice(double price) {
        this.price = Utils.round(price, 2);
    }

    public int getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public int getSinglePackage() {
        return singlePackage;
    }

    public void setSinglePackage(int singlePackage) {
        this.singlePackage = singlePackage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        amount = Utils.round(price * orderedQuantity / singlePackage, 2);
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = Utils.round(amount, 2);
    }
}
