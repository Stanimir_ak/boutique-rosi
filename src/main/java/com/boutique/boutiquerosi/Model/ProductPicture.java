package com.boutique.boutiquerosi.Model;

import jakarta.persistence.*;

@Entity
@Table(name = "product_picture")
public class ProductPicture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "product_id")
    private int productId;

    @Column(name = "pic_num")
    private int picNum;

    @Lob
    @Column(name = "picture", columnDefinition="LONGTEXT")
//    @Column(name = "picture")
    private String picture;

    public ProductPicture() {
    }

    public ProductPicture(int id, int productId, int picNum) {
        this.id = id;
        this.productId = productId;
        this.picNum = picNum;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPicNum() {
        return picNum;
    }

    public void setPicNum(int picNum) {
        this.picNum = picNum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
