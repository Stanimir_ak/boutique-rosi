package com.boutique.boutiquerosi.Model;


public class ShoppingCart {
    private int id;
    private Order order;

    public ShoppingCart(Order order) {
        this.order = order;
    }

    public ShoppingCart() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
