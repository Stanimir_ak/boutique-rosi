package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface CategoryDao extends JpaRepository<Category, Integer>, JpaSpecificationExecutor<Category> {

    @Query(value = " SELECT c from Category c WHERE c.name = :name ")
    Category getOneByName(@Param("name")String name);
}
