package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao extends JpaRepository<Order, Integer>, JpaSpecificationExecutor<Order> {
    @Query(value = " SELECT o FROM Order o WHERE o.isClosed = true ORDER BY o.id DESC ")
    List<Order> getAllClosed();

    @Query(value = " SELECT o FROM Order o WHERE o.isClosed = false ORDER BY o.id DESC ")
    List<Order> getAllOpen();

    @Query(value = " SELECT o FROM Order o ORDER BY o.id DESC")
    List<Order> getLastOrder(Pageable pageable);
}
