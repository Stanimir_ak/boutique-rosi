package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.ProductDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductDTODao extends JpaRepository<ProductDTO, Integer>, JpaSpecificationExecutor<ProductDTO> {

    @Transactional
    @Modifying
    @Query(value = " DELETE from ProductDTO p where p.order.id = :orderId ")
    void deleteProductsFromOrder(@Param("orderId") int orderId);

    @Query(value = " SELECT p FROM ProductDTO p ORDER BY p.id DESC ")
    List<ProductDTO> getProductsOfOrders();
}
