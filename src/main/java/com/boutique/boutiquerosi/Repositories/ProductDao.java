package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

    @Query(value = " SELECT p FROM Product p WHERE p.name = :name ")
    Product getOneByName(@Param("name") String name);

    @Query(value = " SELECT p FROM Product p WHERE p.isHidden = false ORDER BY p.id ASC")
    List<Product> getAllWithoutHidden();

    @Query(value = " SELECT p FROM Product p WHERE p.isHidden = false ORDER BY p.id ASC")
    Slice<Product> getAllSlicedWithoutHidden(Pageable pageable);

    @Query(value = " SELECT p FROM Product p ORDER BY p.id ASC")
    List<Product> getAllWithHidden();

    @Query("SELECT p FROM Product p WHERE p.category.name = :category AND p.isHidden = false")
    Slice<Product> getProductsByCategory(@Param("category") String category, Pageable pageable);

    @Query(value = " SELECT p FROM Product p WHERE p.name LIKE %:keyWords% AND p.isHidden = false ")
    Slice<Product> search(@Param("keyWords") String keyWords, Pageable pageable);

    @Query(value = " SELECT p FROM Product p WHERE p.category.name = :category AND p.name LIKE %:keyWords% AND p.isHidden = false ")
    Slice<Product> getProductsByCategoryAndSearch(@Param("category") String category,
                                                 @Param("keyWords") String keyWords, Pageable pageable);

    @Query(value = " SELECT p FROM Product p WHERE p.isNew = true AND p.isHidden = false ")
    Slice<Product> getAllNew(Pageable pageable);

    @Query(value = " SELECT p FROM Product p WHERE p.isAction = true AND p.isHidden = false ")
    Slice<Product> getAllInAction(Pageable pageable);
}
