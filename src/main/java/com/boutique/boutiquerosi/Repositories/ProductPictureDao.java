package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.ProductPicture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductPictureDao extends JpaRepository<ProductPicture, Integer>,
        JpaSpecificationExecutor<ProductPicture> {

    @Query(value = " SELECT pp FROM ProductPicture pp WHERE pp.productId =:prodId AND pp.picNum =:picNum ")
    ProductPicture getOneByProdIdAndPicNum(int prodId, int picNum);

    @Query(value = " SELECT pp FROM ProductPicture pp WHERE pp.productId =:prodId ORDER BY pp.picNum ASC ")
    List<ProductPicture> getAllPicOfProduct(int prodId);
}
