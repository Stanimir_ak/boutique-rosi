package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {
    @Query(value = " SELECT r from Role r WHERE r.authority = :authority ")
    Role getOneByName(@Param("authority") String authority);
}
