package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.Subcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SubcategoryDao extends JpaRepository<Subcategory, Integer>, JpaSpecificationExecutor<Subcategory> {
    @Query(value = " SELECT s FROM Subcategory s WHERE s.name = :name AND s.category.id = :categoryId ")
    Subcategory getOneByName(@Param("name") String name, @Param("categoryId") int categoryId);
}
