package com.boutique.boutiquerosi.Repositories;

import com.boutique.boutiquerosi.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User,Integer>, JpaSpecificationExecutor<User> {

    @Query(value = " SELECT u from User u WHERE u.username = :username ")
    User getByUsername(@Param("username") String username);
}
