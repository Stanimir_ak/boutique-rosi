package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Product;

import java.util.List;

public interface CategoryService {

    Category getOne(int id);

    Category getOneByName(String name);

    List<Category> getAll();

    List<Product> getProductsByCategory(Category category);

    void createCategory(String name);

    void updateCategory(int id, String name);

    void deleteCategory(String name);

    boolean ifExists(String name);

    void ifNotExistsThrow(String name);
}
