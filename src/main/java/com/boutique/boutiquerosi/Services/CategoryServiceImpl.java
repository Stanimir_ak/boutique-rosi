package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Exceptions.EntityNotFoundException;
import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Repositories.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;

    @Autowired
    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Category getOne(int id) {
        return categoryDao.getOne(id);
    }

    @Override
    public Category getOneByName(String name) {
        return categoryDao.getOneByName(name);
    }

    @Override
    public List<Category> getAll() {
        return categoryDao.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Override
    public List<Product> getProductsByCategory(Category category) {
        return category.getProducts()
                .stream()
                .filter(p -> !p.isHidden())
                .collect(Collectors.toList());
    }

    @Override
    public void createCategory(String name) {
        Category category = new Category(name);
        categoryDao.save(category);
    }

    @Override
    public void updateCategory(int id, String name) {
        Category categoryToUpdate = getOne(id);
        categoryToUpdate.setName(name);
        categoryDao.saveAndFlush(categoryToUpdate);
    }

    @Override
    public void deleteCategory(String name) {
        ifNotExistsThrow(name);
        Category categoryToDelete = getOneByName(name);
        categoryDao.delete(categoryToDelete);
    }

    @Override
    public boolean ifExists(String name) {
        return getOneByName(name) != null;
    }

    @Override
    public void ifNotExistsThrow(String name) {
        if (!ifExists(name)) {
            throw new EntityNotFoundException("category.not.found");
        }
    }
}
