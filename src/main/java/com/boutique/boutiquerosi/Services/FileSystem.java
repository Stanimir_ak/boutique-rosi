package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.*;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;

public interface FileSystem {
    boolean isNotExcel(MultipartFile file);

    void updateAllProductsAndCategories(Principal principal, MultipartFile file) throws IOException;

    Workbook importExcel(Principal principal, MultipartFile file) throws IOException;

    List<Product> getProductsFromExcel(Principal principal, Workbook workbook);

    List<Category> getCategoriesFromExcel(Principal principal, Workbook workbook);

    List<Subcategory> getSubcategoriesFromExcel(Principal principal, Workbook workbook);

    List<Product> getQuantityOfProductsFromExcel(Principal principal, Workbook workbook);

    List<Order> getOrdersFromExcel(Principal principal, Workbook workbook);

    List<ProductDTO> getProductDTOsFromExcel(Principal principal, Workbook workbook);

    void updateProductsButNotQuantity(List<Product> importedList, List<Product> dbList);

    void updateCategories(List<Category> importedList, List<Category> dbList);

    void updateSubcategories(List<Subcategory> importedList, List<Subcategory> dbList);

    void updateProductsQuantity(Principal principal, MultipartFile file) throws IOException;

    void downloadExcelFile(HttpServletResponse response, Principal principal, ExcelType excelType) throws IOException;

    XSSFWorkbook createProductFileExcelFromDB();

    InputStream getInputStreamFromXSSFWorkbook(XSSFWorkbook excel) throws IOException;

    XSSFWorkbook createStatisticsFileExcelFromDB(Principal principal);
}
