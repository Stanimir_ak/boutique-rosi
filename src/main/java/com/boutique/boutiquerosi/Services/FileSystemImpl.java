package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Exceptions.InvalidOperationException;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Utils.Utils;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FileSystemImpl implements FileSystem {
    private UserService userService;
    private MessageService messageService;
    private ProductService productService;
    private CategoryService categoryService;
    private SubcategoryService subcategoryService;
    private OrderService orderService;
    private ProductDTOService productDTOService;

    @Autowired
    public FileSystemImpl(UserService userService, MessageService messageService,
                          ProductService productService, CategoryService categoryService,
                          SubcategoryService subcategoryService, OrderService orderService, ProductDTOService productDTOService) {
        this.userService = userService;
        this.messageService = messageService;
        this.productService = productService;
        this.categoryService = categoryService;
        this.subcategoryService = subcategoryService;
        this.orderService = orderService;
        this.productDTOService = productDTOService;
    }

    @Override
    public void updateAllProductsAndCategories(Principal principal, MultipartFile file) throws IOException {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        if (isNotExcel(file)) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("import.only.excel"));
        }

        List<Product> dbProducts = productService.getAllWithHidden();
        List<Category> dbCategories = categoryService.getAll();
        List<Subcategory> dbSubcategories = subcategoryService.getAll();

        Workbook importedExcel = importExcel(principal, file);

        updateCategories(getCategoriesFromExcel(principal, importedExcel), dbCategories);
        updateSubcategories(getSubcategoriesFromExcel(principal, importedExcel), dbSubcategories);
        updateProductsButNotQuantity(getProductsFromExcel(principal, importedExcel), dbProducts);
    }

    @Override
    public boolean isNotExcel(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        assert fileName != null;
        String[] arr = fileName.split("\\.");
        String fileExtension = arr[arr.length - 1];

        String[] excelExtensions = getExcelExtension();

        for (String extension : excelExtensions) {
            if (extension.equals(fileExtension)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Workbook importExcel(Principal principal, MultipartFile file) throws IOException {
        return new XSSFWorkbook(file.getInputStream());
    }

    @Override
    public List<Category> getCategoriesFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("Category");
        List<Category> categories = new ArrayList<>();
        boolean isFirstRow = true;
        boolean lastRow = false;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            for (Cell cell : row) {
                if (i == 1) {
                    if (cell.getStringCellValue().isEmpty()) {
                        lastRow = true;
                        break;
                    }
                    categories.add(new Category(cell.getStringCellValue()));
                }
                i++;
            }
            if (lastRow) {
                break;
            }
        }
        return categories;
    }

    @Override
    public List<Subcategory> getSubcategoriesFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("Subcategory");
        List<Subcategory> subcategories = new ArrayList<>();
        boolean isFirstRow = true;
        boolean lastRow = false;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            Subcategory subcategory = new Subcategory();
            for (Cell cell : row) {
                switch (i) {
                    case 0:
                        break;
                    case 1:
                        if (cell.getStringCellValue().isEmpty()) {
                            lastRow = true;
                            break;
                        }
                        subcategory.setName(cell.getStringCellValue());
                    case 2:
                        subcategory.setCategory(new Category(cell.getStringCellValue()));
                }

                i++;
            }
            if (lastRow) {
                break;
            }
            subcategories.add(subcategory);
        }
        //TODO remove duplicated
        return subcategories;
    }

    @Override
    @Transactional
    public void updateProductsButNotQuantity(List<Product> importedList, List<Product> dbList) {
        for (Product product : importedList) {
            if (product.getName().isEmpty() || product.getCategory().getName().isEmpty()) {
                break;
            }
            categoryService.ifNotExistsThrow(product.getCategory().getName());
            Category category = categoryService.getOneByName(product.getCategory().getName());
            subcategoryService.ifNotExistsThrow(product.getSubcategory().getName(), category.getId());
            Subcategory subcategory =
                    subcategoryService.getOneByName(product.getSubcategory().getName(), category.getId());

            product.setCategory(category);
            product.setSubcategory(subcategory);

            boolean foundMatch = false;

            for (Product dbProduct : dbList) {
                if (product.getName().equals(dbProduct.getName())) {
                    foundMatch = true;
                    product.setId(dbProduct.getId());

                    //Quantity is not considered part of the product. I am updating it in separate service
                    product.setQuantity(dbProduct.getQuantity());
                    if (!product.equals(dbProduct)) {
                        productService.updateProduct(product);
                    }
                    break;
                }
            }

            if (!foundMatch) {
                productService.createProduct(product);
            }
        }
    }

    @Override
    public List<Product> getQuantityOfProductsFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("Products-Quantity");
        List<Product> products = new ArrayList<>();
        boolean isFirstRow = true;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            Product product = new Product();
            for (Cell cell : row) {
                switch (i) {
                    case 0:
                    case 2:
                    case 3:
                        break;
                    case 1:
                        if (cell.getStringCellValue().isEmpty()) {
                            //last row
                            break;
                        }
                        product.setName(cell.getStringCellValue());
                        break;
                    case 4:
                        product.setQuantity((int) cell.getNumericCellValue());
                        break;
                }
                i++;
                if (i == 5) {
                    break;
                }
            }
            products.add(product);
        }
        return products;
    }

    @Override
    public List<Order> getOrdersFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("Orders");
        List<Order> orders = new ArrayList<>();
        boolean isFirstRow = true;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            Order order = new Order();
            for (Cell cell : row) {
                switch (i) {
                    case 0:
                        order.setId((int) cell.getNumericCellValue());
                        break;
                    case 1:
                        order.setAmount(Utils.round(cell.getNumericCellValue(), 2));
                        break;
                    case 2:
                        order.setCity(cell.getStringCellValue());
                        break;
                    case 3:
                        order.setAddress(cell.getStringCellValue());
                        break;
                    case 4:
                        order.setUserName(cell.getStringCellValue());
                        break;
                    case 5:
                        order.setPhone(cell.getStringCellValue());
                        break;
                    case 6:
                        order.setTransportCosts(Utils.round(cell.getNumericCellValue(), 2));
                        break;
                    case 7:
                        order.setUserId((int) cell.getNumericCellValue());
                        break;
                    case 8:
                        order.setClosed(cell.getBooleanCellValue());
                        break;
                    case 9:
                        order.setDate(cell.getStringCellValue());
                        break;
                    case 10:
                        order.setSize((int) cell.getNumericCellValue());
                        break;
                }
                i++;
                if (i == 11) {
                    break;
                }
            }
            orders.add(order);
        }
        return orders;
    }

    @Override
    public List<ProductDTO> getProductDTOsFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("ProductsOfOrders");
        List<ProductDTO> productDTOList = new ArrayList<>();
        boolean isFirstRow = true;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            ProductDTO product = new ProductDTO();
            for (Cell cell : row) {
                switch (i) {
                    case 0:
                        product.setId((int) cell.getNumericCellValue());
                        break;
                    case 1:
                        product.setProductId((int) cell.getNumericCellValue());
                        break;
                    case 2:
                        product.setName(cell.getStringCellValue());
                        break;
                    case 3:
                        product.setOrder(orderService.getOne((int) cell.getNumericCellValue(), principal));
                        break;
                    case 4:
                        product.setOrderedQuantity((int) cell.getNumericCellValue());
                        break;
                    case 5:
                        product.setPrice(Utils.round(cell.getNumericCellValue(), 2));
                        break;
                    case 6:
                        product.setSinglePackage((int) cell.getNumericCellValue());
                        break;
                }
                i++;
                if (i == 7) {
                    break;
                }
            }
            productDTOList.add(product);
        }
        return productDTOList;
    }

    @Override
    @Transactional
    public void updateCategories(List<Category> importedList, List<Category> dbList) {
        for (Category category : importedList) {
            if (dbList.stream().noneMatch(c -> c.getName().equals(category.getName()))) {
                categoryService.createCategory(category.getName());
            }
        }
    }

    @Override
    @Transactional
    public void updateSubcategories(List<Subcategory> importedList, List<Subcategory> dbList) {
        for (Subcategory subcategory : importedList) {
            categoryService.ifNotExistsThrow(subcategory.getCategory().getName());

            boolean foundMatch = false;

            for (Subcategory dbSubcategory : dbList) {
                if (subcategory.getName().equals(dbSubcategory.getName())) {
                    foundMatch = true;
                    if (!subcategory.equals(dbSubcategory)) {
                        subcategoryService.updateSubcategory(subcategory);
                    }
                    break;
                }
            }

            if (!foundMatch) {
                subcategoryService.createSubcategory
                        (subcategory.getName(), subcategory.getCategory().getName());
            }
        }
    }

    @Override
    public void updateProductsQuantity(Principal principal, MultipartFile file) throws IOException {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        if (isNotExcel(file)) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("import.only.excel"));
        }
        Workbook workbook = importExcel(principal, file);

        List<Product> dbProducts = productService.getAllWithHidden();
        List<Product> importedList = getQuantityOfProductsFromExcel(principal, workbook);

        for (Product importedProduct : importedList) {
            for (Product dbProduct : dbProducts) {
                if (importedProduct.getName().equals(dbProduct.getName())) {
                    dbProduct.setQuantity(importedProduct.getQuantity());
                    productService.updateProduct(dbProduct);
                    break;
                }
            }
        }
    }

    @Override
    public void downloadExcelFile(HttpServletResponse response, Principal principal,
                                  ExcelType excelType) throws IOException {

        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);

        XSSFWorkbook xssfWorkbook;
        if (excelType == ExcelType.ProductFile) {
            xssfWorkbook = createProductFileExcelFromDB();
        } else {
            xssfWorkbook = createStatisticsFileExcelFromDB(principal);
        }

        String fileSaved = saveTheDataInFileAndReturnPath(xssfWorkbook, excelType.name());
        fileSaved = fileSaved.replace("\\", "\\\\");
        String[] fileArr = fileSaved.split("\\\\");
        String fileName = fileArr[fileArr.length - 1];
        String contentType = "application/octet-stream";

        File fileResource = new File(fileSaved);

        byte[] file = Files.readAllBytes(fileResource.toPath());

        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        response.setHeader("charset", "UTF-8");
        response.setContentType(contentType);
        response.setContentLength(file.length);
        response.setStatus(HttpServletResponse.SC_OK);

        OutputStream outputStream;
        try {
            outputStream = response.getOutputStream();
            outputStream.write(file, 0, file.length);
            outputStream.flush();
            outputStream.close();
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    private String saveTheDataInFileAndReturnPath(XSSFWorkbook xssfWorkbook, String fileName) throws IOException {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy--HH-mm-ss");
        String dateString = formatter.format(date);

        String file = getDirPathAndCreateIfNeeded() + "/" + fileName + "-" + dateString + ".xlsx";

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            xssfWorkbook.write(outputStream);
            xssfWorkbook.close();
        } catch (IOException e) {
            try {
                file = file.replace("/", "\\");
                FileOutputStream outputStream = new FileOutputStream(file);
                xssfWorkbook.write(outputStream);
                xssfWorkbook.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
        return file;
    }

    @Override
    public List<Product> getProductsFromExcel(Principal principal, Workbook workbook) {
        Sheet sheet = workbook.getSheet("Products");
        List<Product> products = new ArrayList<>();
        boolean isFirstRow = true;

        for (Row row : sheet) {
            if (isFirstRow) {
                isFirstRow = false;
                continue;
            }

            int i = 0;
            Product product = new Product();
            Category category = new Category();
            for (Cell cell : row) {
                switch (i) {
                    case 0:
                        break;
                    case 1:
                        product.setName(cell.getStringCellValue());
                        break;
                    case 2:
                        category.setName(cell.getStringCellValue());
                        product.setCategory(category);
                        break;
                    case 3:
                        product.setSubcategory(new Subcategory(cell.getStringCellValue(), category));
                        break;
                    case 4:
                        product.setPrice(Utils.round(cell.getNumericCellValue(), 2));
                        break;
                    case 5:
                        product.setSinglePackage((int) cell.getNumericCellValue());
                        break;
                    case 6:
                        product.setInfo(cell.getStringCellValue());
                        break;
                    case 7:
                        product.setNew(cell.getBooleanCellValue());
                        break;
                    case 8:
                        product.setAction(cell.getBooleanCellValue());
                        break;
                    case 9:
                        product.setHidden(cell.getBooleanCellValue());
                        break;
                }
                i++;
                if (i == 11) {
                    break;
                }
            }
            products.add(product);
        }
        return products;
    }

    @Override
    public XSSFWorkbook createStatisticsFileExcelFromDB(Principal principal) {
        //implementation group: 'org.apache.poi', name: 'poi-ooxml', version: '3.17'
        XSSFWorkbook workbook = new XSSFWorkbook();

        List<Order> orders = orderService.getAllClosed(principal);
        List<ProductDTO> productDTOS = productDTOService.getProductsOfOrders();

        createSheetOrders(workbook, orders, "Orders");
        createSheetProductsOfOrders(workbook, productDTOS, "ProductsOfOrders");

        return workbook;
    }

    private void createSheetProductsOfOrders(XSSFWorkbook workbook, List<ProductDTO> productDTOS, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("product_dto_id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("product_id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("name");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("order_id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("orderedQuantity");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(5);
        headerCell.setCellValue("price");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(6);
        headerCell.setCellValue("singlePackage");
        headerCell.setCellStyle(headerStyle);

        int rowNum = 1;
        for (ProductDTO product : productDTOS) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(product.getId());

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(product.getProductId());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(product.getName());

            Cell cell4 = row.createCell(3);
            cell4.setCellValue(product.getOrder().getId());

            Cell cell5 = row.createCell(4);
            cell5.setCellValue(product.getOrderedQuantity());

            Cell cell6 = row.createCell(5);
            cell6.setCellValue(product.getPrice());

            Cell cell7 = row.createCell(6);
            cell7.setCellValue(product.getSinglePackage());

            rowNum++;
        }
    }

    private void createSheetOrders(XSSFWorkbook workbook, List<Order> orders, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("amount");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("city");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("address");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("username");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(5);
        headerCell.setCellValue("phone");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(6);
        headerCell.setCellValue("transport_costs");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(7);
        headerCell.setCellValue("user_id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(8);
        headerCell.setCellValue("is_closed");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(9);
        headerCell.setCellValue("date");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(10);
        headerCell.setCellValue("size");
        headerCell.setCellStyle(headerStyle);

        int rowNum = 1;
        for (Order order : orders) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(order.getId());

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(order.getAmount());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(order.getCity());

            Cell cell4 = row.createCell(3);
            cell4.setCellValue(order.getAddress());

            Cell cell5 = row.createCell(4);
            cell5.setCellValue(order.getUserName());

            Cell cell6 = row.createCell(5);
            cell6.setCellValue(order.getPhone());

            Cell cell7 = row.createCell(6);
            cell7.setCellValue(order.getTransportCosts());

            Cell cell8 = row.createCell(7);
            cell8.setCellValue(order.getUserId());

            Cell cell9 = row.createCell(8);
            cell9.setCellValue(order.isClosed());

            Cell cell10 = row.createCell(9);
            cell10.setCellValue(order.getDate());

            Cell cell11 = row.createCell(10);
            cell11.setCellValue(order.getSize());

            rowNum++;
        }
    }

    @Override
    public XSSFWorkbook createProductFileExcelFromDB() {
        //implementation group: 'org.apache.poi', name: 'poi-ooxml', version: '3.17'
        XSSFWorkbook workbook = new XSSFWorkbook();
        List<Category> categories = categoryService.getAll();
        List<Subcategory> subcategories = subcategoryService.getAll();
        List<Product> products = productService.getAllWithHidden();

        createSheetCategory(workbook, categories, "Category");
        createSheetSubcategory(workbook, subcategories, "Subcategory");
        createSheetProducts(workbook, products, "Products");
        createSheetProductsQuantity(workbook, products, "Products-Quantity");

        return workbook;
    }

    @Override
    public InputStream getInputStreamFromXSSFWorkbook(XSSFWorkbook excel) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        excel.write(bos);
        byte[] barray = bos.toByteArray();
        return new ByteArrayInputStream(barray);
    }

    private void createSheetProductsQuantity(XSSFWorkbook workbook, List<Product> products, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("name");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("category");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("subcategory");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("quantity");
        headerCell.setCellStyle(headerStyle);

        int rowNum = 1;
        for (Product product : products) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(product.getId());

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(product.getName());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(product.getCategory().getName());

            Cell cell4 = row.createCell(3);
            cell4.setCellValue(product.getSubcategory().getName());

            Cell cell5 = row.createCell(4);
            cell5.setCellValue(product.getQuantity());

            rowNum++;
        }
    }

    private void createSheetProducts(XSSFWorkbook workbook, List<Product> products, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("name");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("category");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("subcategory");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("price");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(5);
        headerCell.setCellValue("package");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(6);
        headerCell.setCellValue("info");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(7);
        headerCell.setCellValue("isNew");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(8);
        headerCell.setCellValue("isAction");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(9);
        headerCell.setCellValue("isHidden");
        headerCell.setCellStyle(headerStyle);

        int rowNum = 1;
        for (Product product : products) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(product.getId());

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(product.getName());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(product.getCategory().getName());

            Cell cell4 = row.createCell(3);
            cell4.setCellValue(product.getSubcategory().getName());

            Cell cell5 = row.createCell(4);
            cell5.setCellValue(product.getPrice());

            Cell cell6 = row.createCell(5);
            cell6.setCellValue(product.getSinglePackage());

            Cell cell7 = row.createCell(6);
            cell7.setCellValue(product.getInfo());

            Cell cell8 = row.createCell(7);
            cell8.setCellValue(product.isNew());

            Cell cell9 = row.createCell(8);
            cell9.setCellValue(product.isAction());

            Cell cell10 = row.createCell(9);
            cell10.setCellValue(product.isHidden());

            rowNum++;
        }
    }

    private void createSheetSubcategory(XSSFWorkbook workbook, List<Subcategory> subcategories, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);
        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("name");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("category");
        headerCell.setCellStyle(headerStyle);

        int rowNum = 1;
        for (Subcategory subcategory : subcategories) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(subcategory.getId());

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(subcategory.getName());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(subcategory.getCategory().getName());

            rowNum++;
        }
    }

    private void createSheetCategory(XSSFWorkbook workbook, List<Category> categories, String sheetName) {
        Sheet sheet = workbook.createSheet(sheetName);

        Row headerRow = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = workbook.createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 13);
        font.setBold(false);
        headerStyle.setFont(font);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("id");
        headerCell.setCellStyle(headerStyle);

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("name");
        headerCell.setCellStyle(headerStyle);

        CellStyle style = workbook.createCellStyle();

        int rowNum = 1;
        for (Category category : categories) {
            Row row = sheet.createRow(rowNum);

            Cell cell = row.createCell(0);
            cell.setCellValue(category.getId());
            cell.setCellStyle(style);

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(category.getName());
            cell2.setCellStyle(style);

            rowNum++;
        }
    }

    private String getFilesSystemDir() {
        File file1 = new File("hhhh");
        String absolutePath = file1.getAbsolutePath().replace("hhhh", "");
        return absolutePath.replace("boutique-rosi", "boutique-rosi-files");
    }

    private String getDirPathAndCreateIfNeeded() {
        File filesDir = new File(getFilesSystemDir());
        if (!filesDir.exists()) {
            filesDir.mkdir();
        }
        return filesDir.getAbsolutePath();
    }

    public String[] getExcelExtension() {
        return Constants.EXCEL_EXTENSIONS.split(",");
    }
}
