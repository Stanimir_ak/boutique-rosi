package com.boutique.boutiquerosi.Services;

public interface MessageService {
    String getTranslatedMessage(String msg);
}
