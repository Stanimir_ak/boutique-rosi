package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Model.ShoppingCart;

import java.security.Principal;
import java.util.List;

public interface OrderService {
    Order getOne(int id, Principal principal);

    Order getLastOrder();

    List<Order> getAllOpen(Principal principal);

    List<Order> getAllClosed(Principal principal);

    void create(Order order, Principal principal);

    void closeOrder(Order order, Principal principal);

    void deleteAndReturnStock(Order order, Principal principal);

    boolean isEnoughProductQuantity(ShoppingCart shoppingCart, List<Product> dbProducts);

    void decreaseOrderQuantityWhenNotEnough(ShoppingCart shoppingCart, List<Product> dbProducts);

    void decreaseStockWhenCreateOrder(Order order);
}
