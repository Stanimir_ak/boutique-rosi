package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Model.ProductDTO;
import com.boutique.boutiquerosi.Model.ShoppingCart;
import com.boutique.boutiquerosi.Repositories.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderDao orderDao;
    private final UserService userService;
    private final ProductService productService;
    private final ProductDTOService productDTOService;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, UserService userService, ProductService productService, ProductDTOService productDTOService) {
        this.orderDao = orderDao;
        this.userService = userService;
        this.productService = productService;
        this.productDTOService = productDTOService;
    }

    @Override
    public Order getOne(int id, Principal principal) {
        return orderDao.getOne(id);
    }

    @Override
    public Order getLastOrder() {
        return orderDao.getLastOrder(PageRequest.of(0, 1)).get(0);
    }

    @Override
    public List<Order> getAllOpen(Principal principal) {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        return orderDao.getAllOpen();
    }

    @Override
    public List<Order> getAllClosed(Principal principal) {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        return orderDao.getAllClosed();
    }

    @Override
    public void create(Order order, Principal principal) {
        orderDao.save(order);
        Order createdOrder = getLastOrder();
        productDTOService.createListProductDTO(createdOrder);
        decreaseStockWhenCreateOrder(createdOrder);
    }

    @Override
    public void closeOrder(Order order, Principal principal) {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        order.setClosed(true);
        orderDao.save(order);
    }

    @Override
    public void deleteAndReturnStock(Order order, Principal principal) {
        userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        deleteProductsFromOrder(order);
        orderDao.delete(order);
        returnStockWhenDeleteOrder(order);
    }

    @Override
    public boolean isEnoughProductQuantity(ShoppingCart shoppingCart, List<Product> dbProducts) {
        List<ProductDTO> orderProducts = shoppingCart.getOrder().getProductList();

        for (ProductDTO orderProduct : orderProducts) {
            for (Product dbProduct : dbProducts) {
                if (orderProduct.getProductId() == dbProduct.getId()) {
                    if (orderProduct.getOrderedQuantity() > dbProduct.getQuantity()) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public void decreaseOrderQuantityWhenNotEnough(ShoppingCart shoppingCart, List<Product> dbProducts) {
        List<ProductDTO> orderProducts = shoppingCart.getOrder().getProductList();

        for (ProductDTO orderProduct : orderProducts) {
            for (Product dbProduct : dbProducts) {
                if (orderProduct.getProductId() == dbProduct.getId()) {
                    if (orderProduct.getOrderedQuantity() > dbProduct.getQuantity()) {
                        if (dbProduct.getQuantity() == 0) {
                            shoppingCart.getOrder().getProductList().remove(orderProduct);
                        } else {
                            orderProduct.setOrderedQuantity(dbProduct.getQuantity());
                        }
                    }
                    break;
                }
            }
        }
        shoppingCart.getOrder().calculateAmountAndTransport();
    }

    @Override
    public void decreaseStockWhenCreateOrder(Order order) {
        List<Product> dbProducts = productService.getAllWithoutHidden();
        List<ProductDTO> orderProducts = order.getProductList();

        for (ProductDTO orderProduct : orderProducts) {
            for (Product dbProduct : dbProducts) {
                if (orderProduct.getProductId() == dbProduct.getId()) {
                    dbProduct.setQuantity(dbProduct.getQuantity() - orderProduct.getOrderedQuantity());
                    productService.updateProduct(dbProduct);
                    break;
                }
            }
        }
    }

    private void returnStockWhenDeleteOrder(Order order) {
        List<Product> dbProducts = productService.getAllWithoutHidden();
        List<ProductDTO> orderProducts = order.getProductList();

        for (ProductDTO orderProduct : orderProducts) {
            for (Product dbProduct : dbProducts) {
                if (orderProduct.getProductId() == dbProduct.getId()) {
                    //return means we return quantity in the stock
                    dbProduct.setQuantity(dbProduct.getQuantity() + orderProduct.getOrderedQuantity());
                    productService.updateProduct(dbProduct);
                    break;
                }
            }
        }
    }

    private void deleteProductsFromOrder(Order order) {
        productDTOService.deleteProductsFromOrder(order.getId());
    }
}
