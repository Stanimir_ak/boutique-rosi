package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.ProductDTO;

import java.util.List;

public interface ProductDTOService {

    void createListProductDTO(Order order);

    void deleteProductsFromOrder(int orderId);

    List<ProductDTO> getProductsOfOrders();
}
