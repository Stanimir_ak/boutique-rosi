package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.ProductDTO;
import com.boutique.boutiquerosi.Repositories.ProductDTODao;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductDTOServiceImpl implements ProductDTOService {
    private ProductDTODao productDTODao;

    @Autowired
    public ProductDTOServiceImpl(ProductDTODao productDTODao) {
        this.productDTODao = productDTODao;
    }

    @Override
    @Transactional
    public void createListProductDTO(Order order) {
        for (ProductDTO productDTO : order.getProductList()) {
            productDTO.setOrder(order);
        }
        productDTODao.saveAll(order.getProductList());
    }

    @Override
    public void deleteProductsFromOrder(int orderId) {
        productDTODao.deleteProductsFromOrder(orderId);
    }

    @Override
    public List<ProductDTO> getProductsOfOrders() {
        return productDTODao.getProductsOfOrders();
    }

}
