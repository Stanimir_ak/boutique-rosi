package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.ProductPicture;

import java.util.List;

public interface ProductPictureService {
    ProductPicture getOneById(int id);

    ProductPicture getOneByProdIdAndPicNum(int prodId, int picNum);

    List<ProductPicture> getAllPicOfProduct(int prodId);

    List<ProductPicture> getAll();

    void create(ProductPicture productPicture);

    void update(ProductPicture productPicture);

    void delete(ProductPicture productPicture);

    void deleteAndMoveOtherPics(int id, List<ProductPicture> productPictures);

    List<ProductPicture> makeThisFirstPictureAndReturn(List<ProductPicture> productPictures, int number);

    int getNextNumber(List<ProductPicture> pictures);
}
