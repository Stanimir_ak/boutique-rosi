package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.ProductPicture;
import com.boutique.boutiquerosi.Repositories.ProductPictureDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPictureServiceImpl implements ProductPictureService {

    private final ProductPictureDao productPictureDao;

    @Autowired
    public ProductPictureServiceImpl(ProductPictureDao productPictureDao) {
        this.productPictureDao = productPictureDao;
    }

    @Override
    public ProductPicture getOneById(int id) {
        return productPictureDao.getOne(id);
    }

    @Override
    public ProductPicture getOneByProdIdAndPicNum(int prodId, int picNum) {
        return productPictureDao.getOneByProdIdAndPicNum(prodId, picNum);
    }

    @Override
    public List<ProductPicture> getAllPicOfProduct(int prodId) {
        return productPictureDao.getAllPicOfProduct(prodId);
    }

    @Override
    public List<ProductPicture> getAll() {
        return productPictureDao.findAll(Sort.by(Sort.Direction.ASC, "product_id", "pic_num"));
    }

    @Override
    public void create(ProductPicture productPicture) {
        List<ProductPicture> pictures = getAllPicOfProduct(productPicture.getProductId());

        if (pictures == null || pictures.size() == 0) {
            productPicture.setPicNum(1);
        } else {
            if (isDuplicateNumber(productPicture.getPicNum(), pictures)) {
                productPicture.setPicNum(getNextNumber(pictures));
            }
        }

        productPictureDao.save(productPicture);
    }

    @Override
    public int getNextNumber(List<ProductPicture> pictures) {
        int max = 0;
        for (ProductPicture picture : pictures) {
            if (picture.getPicNum() > max) {
                max = picture.getPicNum();
            }
        }
        return max + 1;
    }

    private boolean isDuplicateNumber(int picNum, List<ProductPicture> pictures) {
        for (ProductPicture picture : pictures) {
            if (picNum == picture.getPicNum()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void update(ProductPicture productPicture) {
        productPictureDao.saveAndFlush(productPicture);
    }

    @Override
    public void delete(ProductPicture productPicture) {
        productPictureDao.delete(productPicture);
    }

    @Override
    public void deleteAndMoveOtherPics(int id, List<ProductPicture> productPictures) {
        ProductPicture picToDelete = getOneById(id);
        int positionOfThePictureToDelete = picToDelete.getPicNum();
        productPictures.remove(picToDelete);

        delete(picToDelete);

        for (ProductPicture picture : productPictures) {
            if (picture.getPicNum() > positionOfThePictureToDelete) {
                picture.setPicNum(picture.getPicNum() - 1);
                productPictureDao.saveAndFlush(picture);
            }
        }
    }

    @Override
    public List<ProductPicture> makeThisFirstPictureAndReturn(List<ProductPicture> productPictures, int number) {
        for (ProductPicture picture : productPictures) {
            if (picture.getPicNum() == number) {
                picture.setPicNum(1);
                break;
            } else {
                picture.setPicNum(picture.getPicNum() + 1);
            }
        }

        for (int i = 0; i < number; i++) {
            productPictureDao.saveAndFlush(productPictures.get(i));
        }

        return productPictures;
    }
}
