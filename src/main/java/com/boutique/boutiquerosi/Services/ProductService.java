package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Page;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Model.ProductDTO;
import com.boutique.boutiquerosi.Model.ShoppingCart;

import org.springframework.data.domain.Slice;

import java.util.List;

public interface ProductService {
    Product getOne(int id);

    Product getOneByName(String name);

    List<Product> getAllWithHidden();

    List<Product> getAllWithoutHidden();

    Slice<Product> getAllSlicedWithoutHidden(Page page);

    Slice<Product> getAllNew(Page page);

    Slice<Product> getAllInAction(Page page);

    Slice<Product> search(String keyWords, Page page);

    Slice<Product> getAllByCategory(String category, Page page);

    Slice<Product> getProductsByCategoryAndSearch(String category, String keyWords, Page page);

    Product createProduct(Product product);

    void updateProduct(Product product);

    void deleteProduct(String name);

    void deleteProductAndItsPictures(int id);

    boolean existsByName(String name);

    void ifNotExistsThrow(String name);

    ProductDTO createDTO(Product product);

    boolean isEnoughProductQuantityOnIncreaseWithOne(int productId, ShoppingCart shoppingCart);
}
