package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Exceptions.EntityNotFoundException;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Model.Page;
import com.boutique.boutiquerosi.Repositories.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductDao productDao;
    private final ProductPictureService productPictureService;

    @Autowired
    public ProductServiceImpl(ProductDao productDao, ProductPictureService productPictureService) {
        this.productDao = productDao;
        this.productPictureService = productPictureService;
    }

    @Override
    public Product getOne(int id) {
        return productDao.getOne(id);
    }

    @Override
    public Product getOneByName(String name) {
        return productDao.getOneByName(name);
    }

    @Override
    public List<Product> getAllWithHidden() {
        return productDao.getAllWithHidden();
    }

    @Override
    public Slice<Product> getAllSlicedWithoutHidden(Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.getAllSlicedWithoutHidden(pageable);
    }

    @Override
    public List<Product> getAllWithoutHidden() {
        return productDao.getAllWithoutHidden();
    }

    @Override
    public Slice<Product> getAllNew(Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.getAllNew(pageable);
    }

    @Override
    public Slice<Product> getAllInAction(Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.getAllInAction(pageable);
    }

    @Override
    public Slice<Product> search(String keyWords, Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.search(keyWords, pageable);
    }

    @Override
    public Slice<Product> getAllByCategory(String category, Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.getProductsByCategory(category, pageable);
    }

    @Override
    public Slice<Product> getProductsByCategoryAndSearch(String category, String keyWords, Page page) {
        Pageable pageable = PageRequest.of(page.getIndex() - 1, Constants.PRODUCTS_PER_PAGE, Sort.by("id").descending());
        return productDao.getProductsByCategoryAndSearch(category, keyWords, pageable);
    }

    @Override
    public Product createProduct(Product product) {
        return productDao.save(product);
    }

    @Override
    public void updateProduct(Product product) {
        productDao.saveAndFlush(product);
    }

    @Override
    public void deleteProduct(String name) {
        ifNotExistsThrow(name);
        Product productToDelete = getOneByName(name);
        productDao.delete(productToDelete);
    }

    @Override
    public void deleteProductAndItsPictures(int id) {
        Product product = productDao.getOne(id);

        List<ProductPicture> pictures = productPictureService.getAllPicOfProduct(id);
        for (ProductPicture picture : pictures) {
            productPictureService.delete(picture);
        }

        productDao.delete(product);
    }

    @Override
    public boolean existsByName(String name) {
        return getOneByName(name) != null;
    }

    @Override
    public void ifNotExistsThrow(String name) {
        if (!existsByName(name)) {
            throw new EntityNotFoundException("product.not.found");
        }
    }

    @Override
    public ProductDTO createDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName(product.getName());
        productDTO.setProductId(product.getId());
        productDTO.setPrice(product.getPrice());
        productDTO.setSinglePackage(product.getSinglePackage());
        productDTO.setOrderedQuantity(product.getSinglePackage());
        return productDTO;
    }

    @Override
    public boolean isEnoughProductQuantityOnIncreaseWithOne(int productId, ShoppingCart shoppingCart) {
        Product dbProduct = getOne(productId);
        boolean isProductInCart = shoppingCart.getOrder().getProductList().stream()
                .anyMatch(p -> p.getProductId() == productId);

        if (isProductInCart) {
            int orderedQuantity = shoppingCart.getOrder().getProductList().stream()
                    .filter(p -> p.getProductId() == productId).findFirst().get().getOrderedQuantity();
            return dbProduct.getQuantity() >= orderedQuantity + dbProduct.getSinglePackage();
        }

        return dbProduct.getQuantity() >= dbProduct.getSinglePackage();
    }
}
