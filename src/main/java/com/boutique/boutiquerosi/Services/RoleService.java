package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Role;

public interface RoleService {
    Role getOne(int id);
    Role getOneByName(String authority);
    boolean existsByName(String authority);
    void create(Role role);
}
