package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Role;
import com.boutique.boutiquerosi.Repositories.RoleDao;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao;

    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Role getOne(int id) {
        return roleDao.getOne(id);
    }

    @Override
    public Role getOneByName(String authority) {
        return roleDao.getOneByName(authority);
    }

    @Override
    public boolean existsByName(String authority) {
        return getOneByName(authority) != null;
    }

    @Override
    public void create(Role role) {
        roleDao.save(role);
    }
}
