package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.Subcategory;

import java.util.List;

public interface SubcategoryService {
    Subcategory getOne(int id);

    Subcategory getOneByName(String name, int categoryId);

    List<Subcategory> getAll();

    void createSubcategory(String subcategory, String category);

    void updateSubcategory(Subcategory subcategory);

    void deleteSubcategory(String name, int categoryId);

    boolean existsByName(String name, int categoryId);

    void ifNotExistsThrow(String name, int categoryId);
}
