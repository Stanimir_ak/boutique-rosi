package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Exceptions.EntityNotFoundException;
import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Subcategory;
import com.boutique.boutiquerosi.Repositories.SubcategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubcategoryServiceImpl implements SubcategoryService {

    private SubcategoryDao subcategoryDao;
    private CategoryService categoryService;

    @Autowired
    public SubcategoryServiceImpl(SubcategoryDao subcategoryDao, CategoryService categoryService) {
        this.subcategoryDao = subcategoryDao;
        this.categoryService = categoryService;
    }

    @Override
    public Subcategory getOne(int id) {
        return subcategoryDao.getOne(id);
    }

    @Override
    public Subcategory getOneByName(String name, int categoryId) {
        return subcategoryDao.getOneByName(name, categoryId);
    }

    @Override
    public List<Subcategory> getAll() {
        return subcategoryDao.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Override
    public void createSubcategory(String subcategoryName, String categoryName) {
        Category category = categoryService.getOneByName(categoryName);
        subcategoryDao.save(new Subcategory(subcategoryName, category));
    }

    @Override
    public void updateSubcategory(Subcategory subcategory) {
        subcategoryDao.saveAndFlush(subcategory);
    }

    @Override
    public void deleteSubcategory(String name, int categoryId) {
        ifNotExistsThrow(name, categoryId);
        Subcategory subcategoryToDelete = getOneByName(name, categoryId);
        subcategoryDao.delete(subcategoryToDelete);
    }

    @Override
    public boolean existsByName(String name, int categoryId) {
        return getOneByName(name, categoryId) != null;
    }

    @Override
    public void ifNotExistsThrow(String name, int categoryId) {
        if (!existsByName(name, categoryId)) {
            throw new EntityNotFoundException("subcategory.not.found");
        }
    }
}
