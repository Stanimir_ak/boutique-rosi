package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Model.User;

import java.security.Principal;
import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    User updateUser(User user);

    void changePassword(Principal principal, User user);

    User getByUsername(String username);

    boolean isAdmin(Principal principal);

    boolean hasAuthority(String username, String authority);

    void ifNotAuthorizedThrow(Principal principal, String authority);
}
