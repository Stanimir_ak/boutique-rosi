package com.boutique.boutiquerosi.Services;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Exceptions.InvalidOperationException;
import com.boutique.boutiquerosi.Model.User;
import com.boutique.boutiquerosi.Repositories.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private MessageService messageService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, MessageService messageService, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.messageService = messageService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.findAll(Sort.by(Sort.Direction.ASC, "username"));
    }

    @Override
    public User updateUser(User user) {
        return userDao.saveAndFlush(user);
    }

    @Override
    public void changePassword(Principal principal, User user) {
        if (principal == null) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("user.pass.changes.invalid"));
        }
        User principalUser = getByUsername(principal.getName());

        if (principalUser == null) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("user.pass.changes.invalid"));
        }

        //Does not work - the encoder generates every time different value, they cannot be compared
        //So the user can change the pass with the same pass - not a big deal
        if (principalUser.getPassword().equals(passwordEncoder.encode(user.getPassword()))) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("user.pass.not.changed"));
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            throw new InvalidOperationException(messageService.getTranslatedMessage("user.pass.does.not.match"));
        }

        userDetailsManager.changePassword(principalUser.getPassword(), passwordEncoder.encode(user.getPassword()));
    }

    @Override
    public User getByUsername(String username) {
        return userDao.getByUsername(username);
    }

    @Override
    public boolean isAdmin(Principal principal) {
        return hasAuthority(principal.getName(), Constants.ROLE_ADMIN);
    }

    @Override
    public boolean hasAuthority(String username, String authority) {
        User user = getByUsername(username);
        return user.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals(authority));
    }

    @Override
    public void ifNotAuthorizedThrow(Principal principal, String authority) {
        if (!hasAuthority(principal.getName(), authority)) {
            throw new AccessDeniedException(messageService.getTranslatedMessage("access.denied"));
        }
    }
}
