package com.boutique.boutiquerosi.Utils;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.Order;
import com.boutique.boutiquerosi.Model.User;

public class Utils {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static boolean isNotValidInput(String input) {
        if (input == null || input.length() == 0) {
            return false;
        }
        input = input.replace(".", "");
        input = input.replace(",", "");
        input = input.replace(":", "");
        input = input.replace("-", "");
        input = input.replace("=", "");

        String[] inputWords = input.split(" ");

        String[] forbiddenWords = Constants.FORBIDDEN_INPUT_WORDS.split(",");
        for (String inputWord : inputWords) {
            for (String forbiddenWord : forbiddenWords) {
                if (inputWord.equalsIgnoreCase(forbiddenWord)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isNotValidUserInfo(User user) {
        return isNotValidInput(user.getUsername()) ||
                isNotValidInput(user.getPassword()) ||
                isNotValidInput(user.getPasswordConfirmation()) ||
                isNotValidInput(user.getEmail());
    }

    public static boolean isNotValidOrderInfo(Order order) {
        return isNotValidInput(order.getAddress()) ||
                isNotValidInput(order.getCity()) ||
                isNotValidInput(order.getPhone()) ||
                isNotValidInput(order.getUserName());
    }
}
