package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Exceptions.EntityNotFoundException;
import com.boutique.boutiquerosi.Model.Category;
import com.boutique.boutiquerosi.Model.Product;
import com.boutique.boutiquerosi.Repositories.CategoryDao;
import com.boutique.boutiquerosi.Services.CategoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl mockedCategoryService;

    @Mock
    CategoryDao categoryDao;

    @Test
    public void getProductsByCategoryShouldReturnNoHiddenProducts() {
        List<Product> products = new ArrayList<>();
        Product lion = Factory.getLion();
        Product product2 = new Product();
        product2.setPrice(1234.50);
        Product product3 = new Product();

        products.add(lion);
        products.add(product2);
        products.add(product3);

        Category category = new Category("Животни");
        category.setProducts(products);

        List<Product> productsResult = mockedCategoryService.getProductsByCategory(category);
        //one of the products is hidden
        assertEquals(2, productsResult.size());
        assertEquals(product2, productsResult.get(0));
        assertEquals(1234.50, productsResult.get(0).getPrice(), 0.0001);
    }

    @Test
    public void ifExistsShouldBeCorrect() {
        Category animals = Factory.getAnimals();
        Mockito.when(categoryDao.getOneByName(animals.getName())).thenReturn(animals);

        assertTrue(mockedCategoryService.ifExists(animals.getName()));
    }

    @Test
    public void ifNotExistsThrow_shouldThrowIfNot() {
        String categoryName = "Животни";
        Mockito.when(categoryDao.getOneByName(categoryName)).thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> mockedCategoryService.ifNotExistsThrow(categoryName));
    }
}
