package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Model.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class Factory {
    public static Product getLion() {
        Product lion = new Product();
        lion.setName("Лъв");
        lion.setQuantity(5);
        lion.setPrice(13.5);
        lion.setId(1);
        Category category = getCategoryAnimals();
        lion.setCategory(category);
        lion.setSubcategory(getSubcategoryCool());
        lion.setAction(false);
        lion.setNew(false);
        lion.setHidden(true);
        lion.setSinglePackage(1);
        lion.setInfo("info");

        return lion;
    }

    public static Product getBird() {
        Product bird = new Product();
        bird.setName("Птица");
        bird.setQuantity(10);
        bird.setPrice(10.5);
        bird.setId(2);
        Category category = getCategoryAnimals();
        bird.setCategory(category);
        bird.setSubcategory(getSubcategoryCool());
        bird.setAction(false);
        bird.setNew(true);
        bird.setHidden(false);
        bird.setSinglePackage(1);
        bird.setInfo("info2");

        return bird;
    }

    public static Category getCategoryAnimals() {
        Category category = new Category("Животни");
        category.setId(1);
        return category;
    }

    public static Category getCategoryPaintings() {
        Category category = new Category("Картини");
        category.setId(2);
        return category;
    }

    public static Subcategory getSubcategoryCool() {
        Subcategory subcategory = new Subcategory("Готини", getCategoryAnimals());
        subcategory.setId(1);
        return subcategory;
    }

    public static Subcategory getSubcategoryBeautiful() {
        Subcategory subcategory = new Subcategory("Красиви", getCategoryAnimals());
        subcategory.setId(2);
        return subcategory;
    }

    public static Category getAnimals() {
        Category animals = new Category("Животни");
        animals.setId(1);
        return animals;
    }

    public static User createUserStan() {
        User stan = new User();
        stan.setUsername("stan");
        return stan;
    }

    public static MultipartFile getMultipartFile(String extension) {
        return new MultipartFile() {
            @Override
            public String getName() {
                return extension;
            }

            @Override
            public String getOriginalFilename() {
                return extension;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };
    }
}
