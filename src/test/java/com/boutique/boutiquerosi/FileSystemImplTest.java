package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Services.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class FileSystemImplTest {

    @InjectMocks
    FileSystemImpl mockedFileSystem;

    @Mock
    UserService userService;

    @Mock
    ProductService productService;

    @Mock
    CategoryService categoryService;

    @Mock
    SubcategoryService subcategoryService;

    @Mock
    OrderService orderService;

    @Mock
    ProductDTOService productDTOService;

    @Test
    public void isNotExcelShouldReturnFalseForXLS() {
        MultipartFile file = Factory.getMultipartFile("file.xls");

        assertFalse(mockedFileSystem.isNotExcel(file));
    }

    @Test
    public void isNotExcelShouldReturnFalseForXLSX() {
        MultipartFile file = Factory.getMultipartFile("file.xlsx");

        assertFalse(mockedFileSystem.isNotExcel(file));
    }

    @Test
    public void isNotExcelShouldReturnTrueForTXT() {
        MultipartFile file = Factory.getMultipartFile("file.txt");

        assertTrue(mockedFileSystem.isNotExcel(file));
    }

    @Test
    public void isNotExcelShouldReturnTrueIfNoExtension() {
        MultipartFile file = Factory.getMultipartFile("file");

        assertTrue(mockedFileSystem.isNotExcel(file));
    }

    @Test
    public void getExcelExtensionShouldReturnTheRightValues() {
        String[] extensions = mockedFileSystem.getExcelExtension();

        assertEquals(2, extensions.length);
        assertEquals("xls", extensions[0]);
        assertEquals("xlsx", extensions[1]);
    }

    @Test
    public void createProductFileExcelFromDB_ShouldCreateCorrectExcelFile() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        Category animals = Factory.getCategoryAnimals();
        Category paintings = Factory.getCategoryPaintings();
        lion.setCategory(animals);
        bird.setCategory(animals);

        Subcategory cool = Factory.getSubcategoryCool();
        Subcategory beautiful = Factory.getSubcategoryBeautiful();

        List<Product> products = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        List<Subcategory> subcategories = new ArrayList<>();

        products.add(lion);
        products.add(bird);

        categories.add(animals);
        categories.add(paintings);

        subcategories.add(cool);
        subcategories.add(beautiful);

        Mockito.when(productService.getAllWithHidden()).thenReturn(products);
        Mockito.when(categoryService.getAll()).thenReturn(categories);
        Mockito.when(subcategoryService.getAll()).thenReturn(subcategories);


//        Mockito.when(userDao.getByUsername(principal.getName())).thenReturn(stan);

        XSSFWorkbook excel = mockedFileSystem.createProductFileExcelFromDB();

        List<Product> productsReturned = mockedFileSystem.getProductsFromExcel(principal, excel);
        List<Category> categoriesReturned = mockedFileSystem.getCategoriesFromExcel(principal, excel);
        List<Subcategory> subcategoriesReturned = mockedFileSystem.getSubcategoriesFromExcel(principal, excel);
        List<Product> quantitiesReturned = mockedFileSystem.getQuantityOfProductsFromExcel(principal, excel);

        assertEquals(2, productsReturned.size());
        assertEquals("Лъв", productsReturned.get(0).getName());

        assertEquals(2, categoriesReturned.size());
        assertEquals("Животни", categoriesReturned.get(0).getName());

        assertEquals(2, subcategoriesReturned.size());
        assertEquals("Готини", subcategoriesReturned.get(0).getName());

        assertEquals(2, quantitiesReturned.size());
        assertEquals("Лъв", quantitiesReturned.get(0).getName());
        assertEquals(5, quantitiesReturned.get(0).getQuantity());
        assertEquals("Птица", quantitiesReturned.get(1).getName());
        assertEquals(10, quantitiesReturned.get(1).getQuantity());
    }

    @Test
    public void updateAllProductsAndCategoriesShouldCallNeededServices() throws IOException {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));

        //making excel file
        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        Category animals = Factory.getAnimals();
        lion.setCategory(animals);
        bird.setCategory(animals);
        Category paintings = Factory.getCategoryPaintings();

        Subcategory cool = Factory.getSubcategoryCool();
        lion.setSubcategory(cool);
        bird.setSubcategory(cool);
        Subcategory beautiful = Factory.getSubcategoryBeautiful();

        List<Product> products = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        List<Subcategory> subcategories = new ArrayList<>();

        products.add(lion);
        products.add(bird);

        categories.add(animals);
        categories.add(paintings);
        Category newCateg = new Category("Нови животни");
        newCateg.setId(3);
        categories.add(newCateg);

        subcategories.add(cool);
        subcategories.add(beautiful);
        Subcategory newSub = new Subcategory("Супер", newCateg);
        newSub.setId(4);
        subcategories.add(newSub);

        Mockito.when(productService.getAllWithHidden()).thenReturn(products);
        Mockito.when(categoryService.getAll()).thenReturn(categories);
        Mockito.when(categoryService.getOneByName(lion.getCategory().getName())).thenReturn(lion.getCategory());
        Mockito.when(categoryService.getOneByName(bird.getCategory().getName())).thenReturn(bird.getCategory());
        Mockito.when(subcategoryService.getAll()).thenReturn(subcategories);
        Mockito.when(subcategoryService.getOneByName(lion.getSubcategory().getName(), lion.getCategory().getId()))
                .thenReturn(lion.getSubcategory());
        Mockito.when(subcategoryService.getOneByName(bird.getSubcategory().getName(), bird.getCategory().getId()))
                .thenReturn(bird.getSubcategory());

        XSSFWorkbook excel = mockedFileSystem.createProductFileExcelFromDB();
        InputStream is = mockedFileSystem.getInputStreamFromXSSFWorkbook(excel);

        MultipartFile file = new MultipartFile() {
            @Override
            public String getName() {
                return "file.xls";
            }

            @Override
            public String getOriginalFilename() {
                return "file.xls";
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return is;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };

        //make db have 1 product less, so that the program will init productService.createProduct(bird) 1 time
        products.remove(bird);
        lion.setPrice(99.99);
        lion.setCategory(newCateg);

        //make db have 1 category less,
        categories.remove(newCateg);

        lion.setSubcategory(newSub);

        //make db have 1 subcategory less,
        subcategories.remove(newSub);

        mockedFileSystem.updateAllProductsAndCategories(principal, file);

        Mockito.verify(userService, Mockito.times(1)).ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        Mockito.verify(productService, Mockito.times(2)).getAllWithHidden();
        Mockito.verify(categoryService, Mockito.times(2)).getAll();
        Mockito.verify(subcategoryService, Mockito.times(2)).getAll();
    }

    @Test
    public void updateProductsQuantity_ShouldCallNeededServices() throws IOException {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));

        //making excel file
        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        Category animals = Factory.getAnimals();
        lion.setCategory(animals);
        bird.setCategory(animals);
        Category paintings = Factory.getCategoryPaintings();

        Subcategory cool = Factory.getSubcategoryCool();
        lion.setSubcategory(cool);
        bird.setSubcategory(cool);
        Subcategory beautiful = Factory.getSubcategoryBeautiful();

        List<Product> products = new ArrayList<>();
        List<Category> categories = new ArrayList<>();
        List<Subcategory> subcategories = new ArrayList<>();

        products.add(lion);
        products.add(bird);

        categories.add(animals);
        categories.add(paintings);
        Category newCateg = new Category("Нови животни");
        newCateg.setId(3);
        categories.add(newCateg);

        subcategories.add(cool);
        subcategories.add(beautiful);
        Subcategory newSub = new Subcategory("Супер", newCateg);
        newSub.setId(4);
        subcategories.add(newSub);

        Mockito.when(productService.getAllWithHidden()).thenReturn(products);
        Mockito.when(categoryService.getAll()).thenReturn(categories);
        Mockito.when(subcategoryService.getAll()).thenReturn(subcategories);

        XSSFWorkbook excel = mockedFileSystem.createProductFileExcelFromDB();
        InputStream is = mockedFileSystem.getInputStreamFromXSSFWorkbook(excel);

        MultipartFile file = new MultipartFile() {
            @Override
            public String getName() {
                return "file.xls";
            }

            @Override
            public String getOriginalFilename() {
                return "file.xls";
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getBytes() throws IOException {
                return new byte[0];
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return is;
            }

            @Override
            public void transferTo(File dest) throws IOException, IllegalStateException {

            }
        };

        //make db have 1 product with different quantity, so that the program will init productService.updateProduct() 1 time
        lion.setQuantity(99);

        mockedFileSystem.updateProductsQuantity(principal, file);

        Mockito.verify(userService, Mockito.times(1)).ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN);
        Mockito.verify(productService, Mockito.times(1)).updateProduct(lion);
    }

    @Test
    public void createStatisticsFileExcelFromDB_shouldCreateCorrectFile() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_USER"));
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);
        Order order2 = new Order(2, 2, "Kolev", "0888777777", "Varna",
                "Ivan Ivanov str", new Date().toString(), 0, 95, false, 4);

        List<Order> orders = new ArrayList<>();
        orders.add(order1);
        orders.add(order2);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        List<ProductDTO> productDTOSOrder2 = new ArrayList<>();
        List<ProductDTO> productDTOSAllOrders = new ArrayList<>();

        ProductDTO product1 = new ProductDTO(order1, 1, 10.0, 1, 1);
        ProductDTO product2 = new ProductDTO(order1, 2, 20.0, 2, 1);
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        ProductDTO product3 = new ProductDTO(order1, 3, 10.0, 1, 1);
        ProductDTO product4 = new ProductDTO(order1, 4, 15.0, 1, 1);
        ProductDTO product5 = new ProductDTO(order1, 5, 50.0, 1, 1);
        ProductDTO product6 = new ProductDTO(order1, 6, 10.0, 2, 1);
        productDTOSOrder2.add(product3);
        productDTOSOrder2.add(product4);
        productDTOSOrder2.add(product5);
        productDTOSOrder2.add(product6);
        order2.setProductList(productDTOSOrder2);

        productDTOSAllOrders.add(product1);
        productDTOSAllOrders.add(product2);
        productDTOSAllOrders.add(product3);
        productDTOSAllOrders.add(product4);
        productDTOSAllOrders.add(product5);
        productDTOSAllOrders.add(product6);

        Mockito.when(orderService.getAllClosed(principal)).thenReturn(orders);
        Mockito.when(productDTOService.getProductsOfOrders()).thenReturn(productDTOSAllOrders);

        XSSFWorkbook excelResult = mockedFileSystem.createStatisticsFileExcelFromDB(principal);

        List<Order> ordersResult = mockedFileSystem.getOrdersFromExcel(principal, excelResult);
        List<ProductDTO> productsResult = mockedFileSystem.getProductDTOsFromExcel(principal, excelResult);

        assertEquals(2, ordersResult.size());
        assertEquals(55.0, ordersResult.get(0).getAmount(), 0.001);
        assertEquals(4, ordersResult.get(1).getSize());

        assertEquals(6, productsResult.size());
        assertEquals(1, productsResult.get(0).getSinglePackage());
        assertEquals(20.0, productsResult.get(1).getPrice(), 0.001);
        assertEquals(2, productsResult.get(5).getOrderedQuantity());
    }
}
