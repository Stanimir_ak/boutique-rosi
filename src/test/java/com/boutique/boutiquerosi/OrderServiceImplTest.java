package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Repositories.OrderDao;
import com.boutique.boutiquerosi.Services.OrderServiceImpl;
import com.boutique.boutiquerosi.Services.ProductDTOService;
import com.boutique.boutiquerosi.Services.ProductService;
import com.boutique.boutiquerosi.Services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

    @InjectMocks
    OrderServiceImpl orderService;

    @Mock
    OrderDao orderDao;

    @Mock
    ProductDTOService productDTOService;

    @Mock
    ProductService productService;

    @Mock
    UserService userService;

    @Test
    public void create_shouldAlsoCreateTheProducts() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        List<Order> dbOrders = new ArrayList<>();
        dbOrders.add(order1);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 1, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 2, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        Mockito.when(orderDao.getLastOrder(PageRequest.of(0, 1))).thenReturn(dbOrders);
        Mockito.when(productService.getAllWithoutHidden()).thenReturn(dbProducts);

        orderService.create(order1, principal);

        Mockito.verify(productDTOService, Mockito.times(1)).createListProductDTO(order1);
    }

    @Test
    public void create_shouldAlsoUpdateDbQuantities() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        List<Order> dbOrders = new ArrayList<>();
        dbOrders.add(order1);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 1, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 2, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        Mockito.when(orderDao.getLastOrder(PageRequest.of(0, 1))).thenReturn(dbOrders);
        Mockito.when(productService.getAllWithoutHidden()).thenReturn(dbProducts);

        orderService.create(order1, principal);

        Mockito.verify(productService, Mockito.times(1)).updateProduct(lion);
        Mockito.verify(productService, Mockito.times(1)).updateProduct(bird);
    }

    @Test
    public void deleteAndReturnStock_shouldDeleteProductDTOsFromOrder() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 1, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 2, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        orderService.deleteAndReturnStock(order1, principal);

        Mockito.verify(productDTOService, Mockito.times(1)).deleteProductsFromOrder(order1.getId());
    }

    @Test
    public void deleteAndReturnStock_shouldReturnStockToDb() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 1, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 2, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        Mockito.when(productService.getAllWithoutHidden()).thenReturn(dbProducts);

        orderService.deleteAndReturnStock(order1, principal);

        Mockito.verify(productService, Mockito.times(1)).updateProduct(lion);
        Mockito.verify(productService, Mockito.times(1)).updateProduct(bird);
    }

    @Test
    public void isEnoughProductQuantity_shouldBeCorrectOnTrue() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 1, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 2, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        ShoppingCart shoppingCart = new ShoppingCart(order1);

        assertTrue(orderService.isEnoughProductQuantity(shoppingCart, dbProducts));
    }

    @Test
    public void isEnoughProductQuantity_shouldBeCorrectOnFalse() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 50, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 50, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        ShoppingCart shoppingCart = new ShoppingCart(order1);

        assertFalse(orderService.isEnoughProductQuantity(shoppingCart, dbProducts));
    }

    @Test
    public void decreaseOrderQuantityWhenNotEnough_ShouldDecreaseWhenNotEnough() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role("ROLE_ADMIN"));

        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 50.0, false, 2);

        Product lion = Factory.getLion();
        Product bird = Factory.getBird();

        List<Product> dbProducts = new ArrayList<>();
        dbProducts.add(lion);
        dbProducts.add(bird);

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 50, lion.getSinglePackage());
        ProductDTO product2 = new ProductDTO(order1, bird.getId(), bird.getPrice(), 50, bird.getSinglePackage());
        productDTOSOrder1.add(product1);
        productDTOSOrder1.add(product2);
        order1.setProductList(productDTOSOrder1);

        ShoppingCart shoppingCart = new ShoppingCart(order1);

        orderService.decreaseOrderQuantityWhenNotEnough(shoppingCart, dbProducts);
        assertEquals(5, product1.getOrderedQuantity());
        assertEquals(10, product2.getOrderedQuantity());
    }
}
