package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Model.ProductPicture;
import com.boutique.boutiquerosi.Repositories.ProductPictureDao;
import com.boutique.boutiquerosi.Services.ProductPictureServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ProductPictureServiceImplTest {

    @InjectMocks
    ProductPictureServiceImpl productPictureService;

    @Mock
    ProductPictureDao productPictureDao;

    @Test
    public void makeThisFirstPictureAndReturn_shouldMakeFirst() {
        List<ProductPicture> productPictures = new ArrayList<>();
        ProductPicture picture1 = new ProductPicture(1, 1, 1);
        ProductPicture picture2 = new ProductPicture(1, 1, 2);
        ProductPicture picture3 = new ProductPicture(1, 1, 3);
        ProductPicture picture4 = new ProductPicture(1, 1, 4);

        productPictures.add(picture1);
        productPictures.add(picture2);
        productPictures.add(picture3);
        productPictures.add(picture4);

        productPictureService.makeThisFirstPictureAndReturn(productPictures, 3);

        assertEquals(1, picture3.getPicNum());
        assertEquals(2, picture1.getPicNum());
        assertEquals(3, picture2.getPicNum());
        assertEquals(4, picture4.getPicNum());
    }
}
