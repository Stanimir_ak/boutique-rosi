package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Model.*;
import com.boutique.boutiquerosi.Repositories.ProductDao;
import com.boutique.boutiquerosi.Services.ProductPictureService;
import com.boutique.boutiquerosi.Services.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductDao productDao;

    @Mock
    ProductPictureService productPictureService;

    @Test
    public void deleteProductAndItsPictures_shouldDeleteAlsoThePictures() {
        Product lion = Factory.getLion();

        List<ProductPicture> productPictures = new ArrayList<>();
        ProductPicture picture1 = new ProductPicture(1, 1, 1);
        ProductPicture picture2 = new ProductPicture(1, 1, 2);

        productPictures.add(picture1);
        productPictures.add(picture2);

        Mockito.when(productDao.getOne(lion.getId())).thenReturn(lion);
        Mockito.when(productPictureService.getAllPicOfProduct(lion.getId())).thenReturn(productPictures);

        productService.deleteProductAndItsPictures(lion.getId());

        Mockito.verify(productPictureService, Mockito.times(1)).delete(picture1);
        Mockito.verify(productPictureService, Mockito.times(1)).delete(picture2);
        Mockito.verify(productDao, Mockito.times(1)).delete(lion);
    }

    @Test
    public void createDTO_shouldCreateMirrorObject() {
        Product lion = Factory.getLion();
        ProductDTO dto = productService.createDTO(lion);

        assertEquals(lion.getName(), dto.getName());
        assertEquals(lion.getId(), dto.getProductId());
        assertEquals(lion.getPrice(), dto.getPrice(), 0.0001);
        assertEquals(lion.getSinglePackage(), dto.getSinglePackage());
        assertEquals(lion.getSinglePackage(), dto.getOrderedQuantity());
    }

    @Test
    public void isEnoughProductQuantityOnIncreaseWithOne_trueWhenThereIs() {
        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 59, false, 1);

        Product lion = Factory.getLion();

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 4, lion.getSinglePackage());
        productDTOSOrder1.add(product1);
        order1.setProductList(productDTOSOrder1);

        ShoppingCart shoppingCart = new ShoppingCart(order1);

        Mockito.when(productDao.getOne(lion.getId())).thenReturn(lion);

        assertTrue(productService.isEnoughProductQuantityOnIncreaseWithOne(lion.getId(), shoppingCart));
    }

    @Test
    public void isEnoughProductQuantityOnIncreaseWithOne_falseWhenThereIsNo() {
        Order order1 = new Order(1, 1, "Stan", "0888555888", "Sofia",
                "Ivan Peichev str", new Date().toString(), 5, 72.5, false, 1);

        Product lion = Factory.getLion();

        List<ProductDTO> productDTOSOrder1 = new ArrayList<>();

        //now the order contains 5 when on stock we have 5, and we try to increase with 1 -> we expect false
        ProductDTO product1 = new ProductDTO(order1, lion.getId(), lion.getPrice(), 5, lion.getSinglePackage());
        productDTOSOrder1.add(product1);
        order1.setProductList(productDTOSOrder1);

        ShoppingCart shoppingCart = new ShoppingCart(order1);

        Mockito.when(productDao.getOne(lion.getId())).thenReturn(lion);

        assertFalse(productService.isEnoughProductQuantityOnIncreaseWithOne(lion.getId(), shoppingCart));
    }
}
