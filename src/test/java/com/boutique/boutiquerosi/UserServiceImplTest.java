package com.boutique.boutiquerosi;

import com.boutique.boutiquerosi.Constants.Constants;
import com.boutique.boutiquerosi.Model.Role;
import com.boutique.boutiquerosi.Model.User;
import com.boutique.boutiquerosi.Repositories.UserDao;
import com.boutique.boutiquerosi.Services.MessageService;
import com.boutique.boutiquerosi.Services.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import java.security.Principal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserDao userDao;

    @Mock
    MessageService messageService;

    @Test
    public void hasAuthority_shouldReturnTrueWhenUserHasTheRole() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role(Constants.ROLE_USER));

        Mockito.when(userDao.getByUsername("stan")).thenReturn(stan);

        assertTrue(userService.hasAuthority("stan", Constants.ROLE_USER));
    }

    @Test
    public void hasAuthority_shouldReturnFalseWhenUserDoesNotHaveTheRole() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role(Constants.ROLE_USER));

        Mockito.when(userDao.getByUsername("stan")).thenReturn(stan);

        assertFalse(userService.hasAuthority("stan", Constants.ROLE_ADMIN));
    }

    @Test
    public void isAdmin_shouldReturnTrueWhenTrue() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role(Constants.ROLE_ADMIN));

        Mockito.when(userDao.getByUsername("stan")).thenReturn(stan);

        assertTrue(userService.isAdmin(principal));
    }

    @Test
    public void isAdmin_shouldReturnFalseWhenNotTrue() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role(Constants.ROLE_USER));

        Mockito.when(userDao.getByUsername("stan")).thenReturn(stan);

        assertFalse(userService.isAdmin(principal));
    }

    @Test
    public void ifNotAuthorizedThrow_shouldThrowIfNotAuthorized() {
        Principal principal = () -> "stan";
        User stan = Factory.createUserStan();
        stan.addAuthority(new Role(Constants.ROLE_USER));

        Mockito.when(userDao.getByUsername("stan")).thenReturn(stan);

        assertThrows(AccessDeniedException.class,
                () -> userService.ifNotAuthorizedThrow(principal, Constants.ROLE_ADMIN));
    }
}
